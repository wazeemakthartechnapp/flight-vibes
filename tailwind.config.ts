import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
    
    },

    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '780px',
      // => @media (min-width: 780px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1465px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1485px',
      // => @media (min-width: 1536px) { ... }
    }
  },
  plugins: [],
}
export default config

