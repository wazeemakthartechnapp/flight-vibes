import { Button, Container } from "@mui/material";
import React from "react";

const Bookaflight = () => {
  return (
    <Container className=" md:mt-32 sm:mt-40 mt-[12rem] rounded-lg sm:px-10">
      <>
        <div
          className=" bg-[url('https://images.unsplash.com/photo-1629065843950-982dfdc9784f?auto=format&fit=crop&q=80&w=1942&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')] 
                  mt-2  bg-no-repeat bg-center bg-cover rounded-lg shadow-lg
                  w-full
                  h-screen 
                  lg:max-h-[40rem] md:max-h-[30rem] max-h-[20rem] 
                  
                  relative
                  -z-10
                 
                  flex
                  flex-col
                  justify-center
                  items-center
                  before:content-['']
                  before:absolute
                  before:inset-0
                  before:block
                  before:bg-gradient-to-r
                before:bg-black from-100%
                  before:rounded-lg
                  before:opacity-50
                  before:z-[-5] "
        >
          {/* <div className=" absolute grid grid-cols-1 place-content-center" >
            <div className=" col-start-3 col-span-7 text-center"> */}
          <h1 className=" md:leading-[3.5rem] md:text-[3rem] text-[1.5rem] leading-1  text-white font-bold text-center">
            Your Journey Begins Here
            <br /> Book Your Flight Now
          </h1>
          {/* </div> */}

          {/* <div className=" col-start-3 col-span-7 text-center"> */}
          <button
            type="button"
            className=" uppercase text-white py-2 rounded-full border-2 md:text-sm sm:text-xs text-[0.5rem] px-6 border-white hover:border-white  bg-black bg-opacity-60 mt-4"
          >
            Book a flight now
          </button>
          {/* </div> */}
          {/* </div> */}
        </div>
      </>
    </Container>
  );
};

export default Bookaflight;
