"use client";
import React, { useState, useEffect } from "react";
import { RxHamburgerMenu, RxDividerVertical } from "react-icons/rx";
import { GoSun, GoMoon } from "react-icons/go";
import { Container, useTheme, ThemeProvider, createTheme } from "@mui/material";
import { FaBars } from "react-icons/fa";
import { RiCloseFill } from "react-icons/ri";
import Image from "next/image";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import PinterestIcon from '@mui/icons-material/Pinterest';

export default function Navbar() {
  const [isDarkMode, setIsDarkMode] = useState(false);
  const [active, setActive] = useState(false);

  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };

  useEffect(() => {
    const htmlElement = document.documentElement;
    if (isDarkMode) {
      htmlElement.classList.add("dark");
    } else {
      htmlElement.classList.remove("dark");
    }
  }, [isDarkMode]);

  const theme = createTheme({
    palette: {
      mode: isDarkMode ? "dark" : "light",
    },
  });

  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  const logos = [
    {
      src: "/Images/logos/ATOL_White.png",
    },
    {
      src: "/Images/logos/travel_aware_sherpa.svg",
    },
    {
      src: "/Images/logos/financial-protection.png",
    },
  ];

  return (
    <ThemeProvider theme={theme}>
      <div className="sticky z-40 top-0 bg-white bg-opacity-30 ">
        <Container>
          <nav className="flex items-center justify-between">
            <div className="mt-1">
              <img src="/Images/Logo.svg" alt="" className=" mt-4" />
            </div>

            <div>
              <ul className="flex items-center sm:gap-4 gap-2">
                <li className="sm:flex hidden">Help</li>
                <li className="sm:flex hidden">Sign in</li>
                <li>
                  <button
                    className="border rounded-full py-2 px-2 text-white font-bold bg-black"
                    onClick={toggleDarkMode}
                  >
                    {isDarkMode ? <GoMoon /> : <GoSun />}
                  </button>
                </li>
                <li>
                  <button className="bg-transparent border-black py-2 px-4 border-2 rounded sm:flex hidden">
                    Book a flight
                  </button>
                </li>
                <li>
                  <RxDividerVertical size="40" className="sm:flex hidden" />
                </li>
                <li>
                  <RxHamburgerMenu
                    size="30"
                    className={`cursor-pointer ${
                      isSidebarOpen ? "rotate-180" : "-rotate-180"
                    }`}
                    onClick={toggleSidebar}
                  />
                </li>
              </ul>
            </div>
          </nav>
        </Container>
        {/* Sidebar */}
        <div
          className={`fixed top-0 right-0 h-full bg-[#E9EFFB] text-black sm:w-64 w-full z-50 overflow-x-hidden transition-all duration-300 ${
            isSidebarOpen ? "translate-x-0" : "translate-x-full"
          }`}
        >
          <div className="flex justify-between items-center pl-4">
            <div className=" pt-4">
              <img src="/Images/Logo.svg" alt="" />
            </div>

            <button className="p-4" onClick={toggleSidebar}>
              {isSidebarOpen ? (
                <RiCloseFill className="text-2xl" />
              ) : (
                <FaBars className="text-2xl" />
              )}
            </button>
          </div>
          <div className="p-4 text-center">
            {/* Sidebar content */}
            <ul>
              <li className="mb-2">
                <button className=" border-2 border-black text-md py-2 px-4 bg-blue-900 text-white rounded-md ">
                  Book a Flight
                </button>
              </li>
              <li className="mb-2">
                <button className=" border-2 border-black text-md py-2 px-11 bg-transparent rounded-md ">
                  Help
                </button>
              </li>
              <li className="mb-2">
                <button className=" border-2 border-black text-md py-2 px-9 bg-transparent rounded-md ">
                  Sign in
                </button>
              </li>
            </ul>
          </div>
          <div>
            <div className=" flex gap-5 justify-center items-center py-3 bg-blue-950 border-y-2 border-y-gray-800">
              {logos.map((image, index) => (
                <Image
                  key={index}
                  width={60}
                  height={60}
                  src={image.src}
                  alt="logo"
                />
              ))}
            </div>

            <div>
              <p className=" text-center text-black border-b px-3 border-b-black py-2">
                Follow Us On
              </p>
              <div className=" flex gap-1 flex-row justify-center items-center mt-3">
                <FacebookIcon fontSize="large" />
                <InstagramIcon fontSize="large"/>
                <YouTubeIcon fontSize="large"/>
                <LinkedInIcon fontSize="large"/>
               <PinterestIcon fontSize="large"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}
