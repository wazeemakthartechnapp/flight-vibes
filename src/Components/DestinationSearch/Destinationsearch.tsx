import { Button, Container } from "@mui/material";
import React from "react";

const Destinationsearch = () => {
  return (
    <React.Fragment>
      <>
        <div
          className="row-span-2 grid p-1  w-full -z-30 mt-12
          
          md:h-[30rem] sm:h-[20rem] h-[21rem] sm:grid-cols-5 grid-cols-2 gap-1 bg-gray-800
             absolute  
               
            "
        >
          <div className="md:row-span-2  sm:row-span-2 row-span-1">
            <img
              className="md:h-[29.5rem] sm:h-[19.5rem] h-[10rem] max-w-full object-cover"
              src="/Images/PopularDestinations/img1.jpg"
              alt=""
            />
          </div>
          <div className="row-span-1 ">
            <img
              className="md:h-[14.5rem] sm:h-[9.5rem] h-[10rem] max-w-full object-cover"
              src="/Images/PopularDestinations/img1.jpg"
              alt=""
            />
          </div>
          <div className="md:row-span-2 ">
            <img
              className="md:h-[29.5rem] sm:h-[9.5rem] h-[10rem]  w-96 object-cover"
              src="/Images/PopularDestinations/img2.jpg"
              alt=""
            />
          </div>
          <div className=" sm:grid hidden">
            <img
              className="md:h-[14.5rem] sm:h-[9.5rem] h-auto  max-w-full object-cover"
              src="/Images/PopularDestinations/img5.jpg"
              alt=""
            />
          </div>
          <div className="sm:row-span-2 md:row-span-2 row-span-1">
            <img
              className="md:h-[29.5rem] sm:h-[19.5rem] h-[10rem] max-w-full object-cover"
              src="/Images/PopularDestinations/img3.jpg"
              alt=""
            />
          </div>
          <div className="sm:grid hidden  ">
            <img
              className="md:h-[14.5rem] h-[9.5rem]  max-w-full object-cover"
              src="/Images/PopularDestinations/img4.jpg"
              alt=""
            />
          </div>

          <div className="sm:grid hidden md:col-span-1 sm:col-span-2 w-full ">
            <img
              className="md:h-[14.5rem] h-[9.5rem] max-w-full object-cover"
              src="/Images/PopularDestinations/img1.jpg"
              alt=""
            />
          </div>
        </div>

        {/* <div
          className="h-3/5 w-full bg-gradient-to-r
                from-emerald-500 from-33.3%
                via-sky-500 via-33.3% 
                to-pink-500 to-33.3% 
                  opacity-50"
        ></div> */}

        <div
          className="
                  mt-12  bg-no-repeat bg-center bg-cover
                  w-full
                  h-[21rem]
                  md:h-[30rem] sm:h-[20rem]
                  py-36
                  relative
                  block
                  -z-10 
                  before:content-['']
                  before:absolute
                  before:inset-0
                  before:block
                  before:bg-gradient-to-r
                before:from-emerald-500 from-33.3%
                before:via-sky-500 via-33.3% 
                before:to-pink-500 to-33.3% 
                  before:opacity-50
                  before:z-[-5]"
        >
          {" "}
          <div className=" flex flex-col justify-center items-center md:gap-14 gap-7 md:mt-auto sm:-mt-16">
         
              <p className=" text-white font-bold md:text-3xl sm:text-2xl text-base  text-center leading-snug md:w-[61rem]">
                Do you have a spacific destination in mind,
                <br/>or are you open to suggestions?
              </p>
            

            <button className=" text-center uppercase text-white  bg-emerald-500 hover:bg-emerald-700 px-12 rounded-md py-2">
              Search
            </button>
          </div>
        </div>
      </>

      <Container className=" mt-16">
        <div className=" grid grid-cols-1 ">
          <div className=" text-left">
            <h1 className=" sm:text-4xl text-2xl sm:text-left text-center  font-bold">
              How to Book a flight ticket with Flight Vides{" "}
            </h1>
          </div>

          <div className=" text-left mt-4">
            <p className=" font-medium sm:text-base text-sm">
              To find the right air ticket for your trip, just enter the
              location you&apos;re flying from and your flight destination.
              Enter your dates, class of travel and the number of passengers.
              Select &apos;Search flights&apos; to continue with the online
              flight booking process and book a flight ticket that suits your
              travel plans.
              <br />
              <br />
              Look out for our Best Price Guarantee mark to make sure
              you&apos;re getting the best flight ticket price when you book a
              flight with Flight vibes.
            </p>
          </div>
        </div>
      </Container>
    </React.Fragment>
  );
};

export default Destinationsearch;
