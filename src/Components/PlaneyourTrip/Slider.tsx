import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Link } from "@mui/material";
import { Container } from "postcss";

type FlightData = {
  src: string;
  destination: string;
  price: number;
  route: string;
  duration: string;
  button: string;
  backgroundColor?: string;
};

const Slider = () => {
  const flights: FlightData[] = [
    {
      src: "/Images/Trippalne/img1.jpg",
      destination: "California",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Business Class",
      backgroundColor: "#B27CDD",
    },
    {
      src: "/Images/Trippalne/img2.jpg",
      destination: "Uganda",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Ecconomy Class",
      backgroundColor: "#00C590",
    },
    {
      src: "/Images/Trippalne/img3.jpg",
      destination: "New York",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Ecconomy Class",
      backgroundColor: "#00C590",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    // Add data for the other flights here
  ];

  return (
    <>
      <Swiper
        spaceBetween={20}
        slidesPerView={1}
        loop={true}
        navigation
        pagination={{ clickable: true }}
      >
        {flights.map((flight, index) => (
          <SwiperSlide key={index}>
            <div className="flight-card h-[13rem] flex items-end ">
              <img
                src={flight.src}
                alt={flight.destination}
                className="flight-image rounded-md object-cover h-52 w-full absolute "
              />
              <div className="flight-details relative p-2 w-full rounded-md  bg-gradient-to-t from-black from-30% bg-opacity-30">
                <div className="flex justify-between">
                  <h3 className="mb-2 text-2xl text-white font-semibold tracking-tight">
                    {flight.destination}
                  </h3>
                  <p className="mb-2 text-2xl font-semibold tracking-tight text-white">
                    £ {flight.price}
                  </p>
                </div>

                <div className=" flex justify-between">
                  <p className="mb-3 text-white font-normal ">{flight.route}</p>
                  <p className="mb-3 text-white font-normal ">
                    {flight.duration}
                  </p>
                </div>

                <button
                  className="flight-button flex justify-center items-center px-3 py-2  text-sm font-medium text-center text-white rounded-lg w-full "
                  style={{ backgroundColor: flight.backgroundColor }}
                >
                  {flight.button}
                </button>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
      
    </>
  );
};

export default Slider;
