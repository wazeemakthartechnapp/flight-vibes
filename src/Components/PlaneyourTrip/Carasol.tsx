// components/Carousel.tsx
"use client";
import { Container, Link, Typography } from "@mui/material";
import React from "react";
import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import Slider from "./Slider";

type FlightData = {
  src: string;
  destination: string;
  price: number;
  route: string;
  duration: string;
  button: string;
  backgroundColor?: string;
};

  const flights: FlightData[] = [
    {
      src: "/Images/Trippalne/img1.jpg",
      destination: "California",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Business Class",
      backgroundColor: "#B27CDD",
    },
    {
      src: "/Images/Trippalne/img2.jpg",
      destination: "Uganda",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Ecconomy Class",
      backgroundColor: "#00C590",
    },
    {
      src: "/Images/Trippalne/img3.jpg",
      destination: "New York",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "Ecconomy Class",
      backgroundColor: "#00C590",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    {
      src: "/Images/Trippalne/img4.jpg",
      destination: "Disney Land",
      price: 478,
      route: "colombo - Riga",
      duration: "29h 30m",
      button: "First Class",
      backgroundColor: "#1B4596",
    },
    // Add data for the other flights here
  ];

  const Carousel: React.FC = () => {
    const handleScroll = (direction: "left" | "right") => {
      const container = document.getElementById("carousel-container");

      if (container) {
        const scrollAmount = direction === "left" ? -600 : 600;
        container.scrollLeft += scrollAmount;
      }
    };

    return (
      <React.Fragment>
         <Container className=" mt-10 mb-4 ">
        <div className=" flex justify-between items-center">
          <div>
            <h1 className="md:text-4xl sm:text-3xl text-2xl font-bold">Plane your next trip</h1>
          </div>
          <div className=" sm:flex hidden">
            <Link
              className=" uppercase flex items-center font-bold"
              underline="none"
            >
              view all deals <AiOutlineRight className="" />
            </Link>
          </div>
        </div>
      </Container>
      
      <Container className=" sm:block hidden">
        <div className="relative mx-auto max-w-full ">
        <button
        className="absolute left-0 top-1/2 transform -translate-y-1/2 bg-[#00C590] text-3xl  rounded-full py-3 px-3 text-white font-bold cursor-pointer z-1"
        style={{ marginLeft: '-25px' }} // Adjust as needed
        onClick={() => handleScroll('left')}
      >
       <AiOutlineLeft/>
      </button>

      <button
        className="absolute right-0 top-1/2 transform -translate-y-1/2 bg-[#00C590] text-3xl text-white px-3 py-3 rounded-full font-bold cursor-pointer z-1"
        style={{ marginRight: '-25px' }} // Adjust as needed
        onClick={() => handleScroll('right')}
      >
        <AiOutlineRight/>
      </button>
        
      <div className="relative mx-auto max-w-full  overflow-hidden -z-50">
        <style jsx>{`
          #carousel-container::-webkit-scrollbar {
            display: none;
          }
        `}</style>

        <div
          id="carousel-container"
          className="flex gap-4 overflow-x-scroll scroll-smooth snap-x-mandatory whitespace-nowrap relative"
        >
          {flights.map((flight, index) => (
            <div
              key={index}
              className=" max-h-full w-80 rounded-b-lg shadow bg-gray-200"
            >
              <img
                className="rounded-b-full max-h-96 w-full "
                src={flight.src}
                alt=""
              />
              <div className=" flex top-3 mx-3 absolute items-center">
                <button
                  type="button"
                  className=" bg-white px-3 py-1 rounded-md"
                >
                  Direct
                </button>
                <>
                  <FavoriteBorderOutlinedIcon
                    fontSize="medium"
                    className=" fill-red-900 bg-white rounded-full px-1 py-1 ml-[12.5rem] hover:cursor-pointer"
                  />
                </>
              </div>
              <div className=" px-5 pb-5 pt-2 w-80">
                <div className="flex justify-between">
                  <h5 className="mb-2 text-2xl font-semibold tracking-tight">
                    {flight.destination}
                  </h5>
                  <Typography className="mb-2 text-2xl font-semibold tracking-tight text-blue-800">
                    £ {flight.price}
                  </Typography>
                </div>
                <div className="flex justify-between">
                  <p className="mb-3 font-normal ">{flight.route}</p>
                  <Typography className="mb-3 font-normal ">
                    {flight.duration}
                  </Typography>
                </div>
                <button
                  type="button"
                  style={{ backgroundColor: flight.backgroundColor }}
                  className="flex justify-center items-center px-3 py-2  text-sm font-medium text-center text-white rounded-lg w-full "
                >
                  {flight.button}
                </button>
              </div>
            </div>
          ))}
        
        </div>

 
      </div>
      </div>

      </Container>
      <Container className="sm:hidden block">
        <Slider/>
      </Container>
      </React.Fragment>
    );
  };

export default Carousel;
