import {
  Box,
  Button,
  Container,
  Grid,
  TextField ,
  Typography,
} from "@mui/material";
import React from "react";

const Membersonly = () => {
  return (
    <Box
      sx={{
        display: "flex",
        backgroundColor: "#F6EDFC",
        alignItems: "center",
        justifyContent: "center",
        marginTop: "100px",
        paddingY: "40px",
      }}
    >
      <Container className=" -mt-3">
        <div className=" grid grid-cols-12 gap-4">
          <div className=" lg:col-start-5 lg:col-span-5 md:col-span-12 col-span-12 text-center">
            <Typography color="#B37DDD" className=" font-bold lg:text-6xl text-4xl ">
              Members Only
            </Typography>
          </div>

          <div className=" col-start-1 col-span-12 text-center">
            <Typography>
              Receive tailored offers, tips & travel ideas from World Flight
              Vibes. Don&apos;t worry, we <br/> don&apos;t spam and you can opt
              out at any time. Learn More
            </Typography>
          </div>
        </div>

        <div className=" grid grid-cols-12 gap-4  mt-4 lg:ml-24">
          <div className=" lg:col-start-3 lg:col-span-5 md:col-span-12 col-span-12">
            <div className=" flex justify-center items-center">
              <input
                type="email"
                id="email"
                className=" h-16 border border-gray-300 text-lg rounded block w-full p-2.5 pl-5 bg-violet-100 dark:placeholder-gray-400 placeholder:font-semibold"
                placeholder="Jhondoe@example.com"
                required
              />
            </div>
          </div>

          <div className=" lg:col-start-8 lg:col-span-2 col-start-1 text-center col-span-12 ">
            <Button
              fullWidth
              className=" uppercase  bg-[#B37DDD] text-white hover:bg-violet-500 h-16"
            >
              Subscribe
            </Button>
          </div>
        </div>
      </Container>

      {/* <Grid container spacing={2} direction="column">
        <Grid item md={12} textAlign="center">
          <Typography variant="h3" color="#B37DDD">
            Members only
          </Typography>
        </Grid>

        <Grid item md={4} textAlign="center">
          <p>
            Receive tailored offers, tips & travel ideas from World Flight
            Vibes. Don&apos;t worry, we don&apos;t spam and you can opt out at
            any time.{" "}
          </p>
        </Grid>

        <Grid container item md={8}>
          <Grid item md={6}>
            <TextField
              id="outlined-basic"
              label="Outlined"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item md={4}>
            <Button fullWidth>Subscribe</Button>
          </Grid>
        </Grid>
      </Grid> */}
    </Box>
  );
};

export default Membersonly;
