import { Container } from "@mui/material";
import React from "react";
import Swiper from "swiper";
import { SwiperSlide } from "swiper/react";

const Popular = () => {
  return (
    <Container className=" mt-16">
      <div className="grid grid-cols-1">
        <div className=" col-start-1 ">
          <h1 className="sm:text-4xl text-3xl font-bold"> Popular Destinations </h1>
        </div>
      </div>

      <div className="lg:grid hidden mt-10 w-full max-h-[30rem] grid-cols-9 gap-2">
        <div className=" col-span-4 grid grid-cols-1 justify-items-center">
          <div className=" absolute flex ">
            <button className=" z-10 relative bg-white rounded-md xl:w-[30.5rem] min-w-[25rem] mt-36 h-20 text-3xl font-semibold">
              Glasgow
            </button>
          </div>
          <img
            className=" h-[14.5rem] w-screen max-w-full rounded-md object-cover"
            src="/Images/PopularDestinations/img1.jpg"
            alt=""
          />
        </div>
        <div className="row-span-1 col-span-2 ">
          <div className=" absolute ">
            <button className=" z-10 relative bg-white rounded-md xl:w-[14.5rem] w-48 ml-2 mt-36 h-20 text-3xl font-semibold">
              Paris
            </button>
          </div>
          <img
            className="h-[14.5rem]  w-screen max-w-full rounded-md object-cover"
            src="/Images/PopularDestinations/img2.jpg"
            alt=""
          />
        </div>
        <div className="row-span-1 col-span-3 flex justify-center items-end">
          <div className=" absolute flex ">
            <button className=" z-10 relative bg-white rounded-md xl:w-[22.5rem] w-[18.5rem] mb-2 h-20 text-3xl font-semibold">
              Nantes
            </button>
          </div>
          <img
            className="h-[29rem] w-96 rounded-md object-cover"
            src="/Images/PopularDestinations/img3.jpg"
            alt=""
          />
        </div>
        <div className="row-span-1 col-span-3 flex justify-center items-end">
          <div className=" absolute flex ">
            <button className=" z-10 relative bg-white rounded-md xl:w-[22.5rem] w-[18.5rem] mb-2 h-20 text-3xl font-semibold">
              Dortmund
            </button>
          </div>
          <img
            className="h-[24rem] w-screen max-w-full -mt-[14.5rem] rounded-md  object-cover"
            src="/Images/PopularDestinations/img4.jpg"
            alt=""
          />
        </div>
        <div className="row-span-1 col-span-3 flex justify-center items-end">
          <div className=" absolute flex ">
            <button className=" z-10 relative bg-white rounded-md xl:w-[22.5rem] w-[18.5rem] mb-2 h-20 text-3xl font-semibold">
              Nantes
            </button>
          </div>
          <img
            className="h-[24rem] max-w-full -mt-[14.5rem] rounded-md object-cover"
            src="/Images/PopularDestinations/img5.jpg"
            alt=""
          />
        </div>
        <div className="row-span-1 col-span-3 flex items-end ">
          <button
            className="h-[9.5rem] w-screen max-w-full rounded-md 
            bg-gradient-to-bl
            from-pink-400 from-33.3%
            via-blue-800 via-33.3% 
            to-emerald-400 to-33.3% 
                       text-white text-xl font-medium "
          >
            {" "}
            View All Destinations
          </button>
        </div>
      </div>

      <div className="">
        <div className=" lg:hidden  sm:grid hidden grid-cols-2 grid-rows-2 gap-5 mt-4">
          <div className=" flex items-end justify-center flex-grow">
            <div className=" absolute flex">
              <button className=" z-10 relative bg-white rounded-md w-80 mb-[0.7rem] h-20 text-3xl font-semibold">
                Glasgow
              </button>
            </div>
            <img
              className=" h-full w-screen max-w-full rounded-md object-cover"
              src="/Images/PopularDestinations/img1.jpg"
              alt=""
            />
          </div>
          <div className=" row-span-2 flex items-end justify-center">
            <div className=" absolute flex ">
              <button className=" z-10 relative bg-white rounded-md w-80 mb-[0.7rem] h-20 text-3xl font-semibold">
                Paris
              </button>
            </div>
            <img
              className="h-full  w-screen max-w-full rounded-md object-cover"
              src="/Images/PopularDestinations/img2.jpg"
              alt=""
            />
          </div>
          <div className=" flex items-end justify-center">
            <div className=" absolute flex ">
              <button className=" z-10 relative bg-white rounded-md w-[20rem] max-h-full mb-[0.7rem] h-20 text-3xl font-semibold">
               Dortmund
              </button>
            </div>
            <img
              className="h-full w-screen max-w-full  rounded-md object-cover"
              src="/Images/PopularDestinations/img4.jpg"
              alt=""
            />
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Popular;
