"use client";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Container, Link } from "@mui/material";

const PopularPhone: React.FC = () => {
  return (
    <React.Fragment>
      <Container className="sm:hidden block mt-4">
        <Swiper
          spaceBetween={50}
          slidesPerView={1}
          loop={true}
          navigation
          pagination={{ clickable: true }}
        >
          <SwiperSlide>
            <div className=" flex flex-col gap-4">
              <div className=" flex justify-end items-end">
                <img
                  src="/Images/PopularDestinations/img1.jpg"
                  alt=""
                  className=" rounded-md object-cover h-full"
                />
                <div className=" p-2 z-10 absolute w-full flex-shrink ">
                  <button className="font-semibold  text-center rounded-md w-full bg-white py-3">
                    Glasgow
                  </button>
                </div>
              </div>

              <div className=" flex justify-end items-end">
                <img
                  src="/Images/PopularDestinations/img3.jpg"
                  alt=""
                  className=" rounded-md object-cover h-full"
                />

                <div className=" p-2 z-10 absolute w-full flex-shrink ">
                  <button className="font-semibold  text-center rounded-md w-full bg-white py-3">
                    Nantes
                  </button>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className=" flex flex-col gap-4">
              <div className=" flex justify-end items-end">
                <img
                  src="/Images/PopularDestinations/img4.jpg"
                  alt=""
                  className=" rounded-md"
                />
                <div className=" p-2 z-10 absolute w-full flex-shrink ">
                  <button className="font-semibold  text-center rounded-md w-full bg-white py-3">
                    Dortmund
                  </button>
                </div>
              </div>

              <div className=" flex justify-end items-end">
                <img
                  src="/Images/PopularDestinations/img5.jpg"
                  alt=""
                  className=" rounded-md"
                />

                <div className=" p-2 z-10 absolute w-full flex-shrink ">
                  <button className=" font-semibold  text-center rounded-md w-full bg-white py-3">
                    Nantes
                  </button>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className=" flex justify-end items-end">
              <img
                src="/Images/PopularDestinations/img2.jpg"
                alt=""
                className=" rounded-md"
              />

              <div className=" p-2 z-10 absolute w-full flex-shrink ">
                <button className=" font-semibold  text-center rounded-md w-full bg-white py-3">
                  Nantes
                </button>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </Container>
      <div className=" px-[1.1rem] justify-end items-end sm:hidden flex">
        <Link
          href={""}
          className=" uppercase text-left text-xs font-bold"
          underline="none"
        >
          View All Destinatios
        </Link>
      </div>
    </React.Fragment>
  );
};

export default PopularPhone;
