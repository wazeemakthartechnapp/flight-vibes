import { Button, Container, Link } from "@mui/material";
import Accordion from "./Faq";
import React from "react";

export const metadata = {
  title: "Animated Accordion - Cruip Tutorials",
  description: "Page description",
};

export default function AnimatedAccordionPage() {
  const faqs = [
    {
      title: "When is the best time to buy plane tickets?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
    {
      title: "How can I save on airline tickets?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
    {
      title: "What are the best tips for booking cheap flights?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
    {
      title: "How can I save money when i fly often?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
    {
      title: "How do I get the best airfare prices?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
    {
      title: "Is booking a multi-flight cheaper?",
      text: "If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.",
      active: false,
    },
  ];

  return (
    <Container className=" items-start mt-11 lg:mt-[12rem]">
      <div className=" grid grid-cols-1 ">
        <div className=" text-center mb-5">
          <h1 className=" text-4xl font-bold">Why Flight vibes</h1>
        </div>

        <div className=" col-start-1 col-span-12 text-left">
          <p className=" font-medium text-base">
            Flights to Suit YouReady to jet off? Booking a flight is the first
            step to an exciting getaway.wever, with so many things to consider,
            finding cheap flights is not always the easiest of tasks.
            That&apos;s why, at flight vibes, we&apos;ve made things simple.
            Letting you search and compare flights from thousands of different
            airlines to destinations all over the world, it&apos;s never been
            easier to find flight tickets to suit you. From economy to
            first-class airline tickets, direct to connecting flights and return
            and one-way tickets, you can filter your search to suit....
          </p>
        </div>
        <div className=" text-right mb-5 col-span-12 ">
          <Link className=" cursor-pointer text-sm font-bold" underline="none">
            Read more
          </Link>
        </div>
      </div>

      <h1 className=" text-4xl font-bold">Freqantly asked questions</h1>

      <div className="grid lg:grid-cols-2 grid-cols-1 gap-x-7 gap-y-3 w-full mt-6">
        {faqs.map((faq, index) => (
          <div className="bg-[#F8F7F7] px-4 flex items-start">
            <Accordion
              key={index}
              title={faq.title}
              id={`faqs-${index}`}
              active={faq.active}
            >
              {faq.text}
            </Accordion>
          </div>
        ))}
      </div>

      <div className="grid grid-cols-12 mt-3">
        <div className=" col-start-4 col-span-6 lg:col-start-6 md:col-start-5 lg:col-span-2 md:col-span-4">
          <Button
            fullWidth
            className=" text-center bg-blue-900 normal-case hover:bg-blue-800 text-white h-12"
          >
            {" "}
            More FAQ
          </Button>
        </div>
      </div>
    </Container>
  );
}
