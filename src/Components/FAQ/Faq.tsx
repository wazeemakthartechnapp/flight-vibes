'use client'

import { useState, useEffect } from 'react'

type AccordionpProps = {
  children: React.ReactNode
  title: string
  id: string,
  active?: boolean
}

export default function Accordion({
  children,
  title,
  id,
  active = false
}: AccordionpProps) {

  const [accordionOpen, setAccordionOpen] = useState<boolean>(false)

  useEffect(() => {
    setAccordionOpen(active)
  }, [])

  return (
    <div className="py-2">
      <h2>
        <button
          className="flex items-center justify-between w-full text-left py-2 bg-[#F8F7F7]"
          onClick={(e) => { e.preventDefault(); setAccordionOpen(!accordionOpen); }}
          aria-expanded={accordionOpen}
          aria-controls={`accordion-text-${id}`}
        >
          <span>{title}</span>
          <svg
          className={`w-6 h-6 transition-transform transform ${
            accordionOpen ? 'rotate-180' : ''
          }`}
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d={accordionOpen ? 'M5 15l7-7 7 7' : 'M19 9l-7 7-7-7'}
          />
        </svg>
        </button>
      </h2>
      <div
        id={`accordion-text-${id}`}
        role="region"
        aria-labelledby={`accordion-title-${id}`}
        className={`grid text-sm text-slate-600 overflow-hidden transition-all duration-300 ease-in-out ${accordionOpen ? 'grid-rows-[1fr] opacity-100' : 'grid-rows-[0fr] opacity-0'}`}
      >
        <div className="overflow-hidden">
          <p className="pb-3">
            {children}
          </p>
        </div>
      </div>
    </div>
  )
}







































// 'use client'
// import { Button, Container, Link } from "@mui/material";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
// import React, { useEffect, useState } from "react";

// const Faq = () => {
//   const [accordionOpen, setAccordionOpen] = useState<boolean>(false)

//   useEffect(() => {
//     setAccordionOpen(false)
//   }, [])
  
//   return (
//     <Container className=" mt-16">
      // <div className=" grid grid-cols-1 ">
      //   <div className=" text-start mb-5">
      //     <h1 className=" text-4xl font-bold">Why Flight vibes</h1>
      //   </div>

      //   <div className=" col-start-1 col-span-12 text-left">
      //     <p className=" font-medium text-base">
      //       Flights to Suit YouReady to jet off? Booking a flight is the first
      //       step to an exciting getaway.wever, with so many things to consider,
      //       finding cheap flights is not always the easiest of tasks.
      //       That&apos;s why, at flight vibes, we&apos;ve made things simple.
      //       Letting you search and compare flights from thousands of different
      //       airlines to destinations all over the world, it&apos;s never been
      //       easier to find flight tickets to suit you. From economy to
      //       first-class airline tickets, direct to connecting flights and return
      //       and one-way tickets, you can filter your search to suit....
      //     </p>
      //   </div>
      //   <div className=" text-right mb-5 col-span-12 ">
      //     <Link className=" cursor-pointer text-sm font-bold" underline="none">
      //       Read more
      //     </Link>
      //   </div>
      // </div>

//       <div className="">
//         <h1 className=" text-4xl font-bold">Freqantly asked questions</h1>
//       </div>
//       <div className=" grid grid-cols-12 gap-x-4 gap-y-2 mt-6 sm:grid-cols-12">
//         <button className="flex col-start-1 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 bg-[#F8F7F7] flex-row justify-between items-center h-14" 
//         onClick={(e) => { e.preventDefault(); setAccordionOpen(!accordionOpen); }}
//         aria-expanded={accordionOpen}
//         aria-controls={`accordion-text-01`}
//         >
//           <p className=" ml-3 sm:text-sm">When is the best time to buy plane tickets?</p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>
//         <div
//         id={`accordion-text-01`}
//         role="region"
//         aria-labelledby={`accordion-title-01`}
//         className={`grid col-start-1 col-span-6 text-sm p-4 text-slate-600 bg-[#F8F7F7] overflow-hidden transition-all duration-300 ease-in-out ${accordionOpen ? 'grid-rows-[1fr] opacity-100' : 'grid-rows-[0fr] opacity-0'}`}
//       >
//         <div className="overflow-hidden">
//           <p className="pb-3">
//             If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.
//           </p>
//         </div>
//        </div>





//         <button className="col-start-1 lg:col-start-7 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 bg-[#F8F7F7] flex flex-row justify-between items-center h-14"
//          onClick={(e) => { e.preventDefault(); setAccordionOpen(!accordionOpen); }}
//          aria-expanded={accordionOpen}
//          aria-controls={`accordion-text-01`}
//          >
//           <p className=" ml-3">How can I save on airline tickets?</p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>
//         <div
//         id={`accordion-text-01`}
//         role="region"
//         aria-labelledby={`accordion-title-01`}
//         className={`grid col-start-7 col-span-6 text-sm p-4 text-slate-600 bg-[#F8F7F7] overflow-hidden transition-all duration-300 ease-in-out ${accordionOpen ? 'grid-rows-[1fr] opacity-100' : 'grid-rows-[0fr] opacity-0'}`}
//       >
//         <div className="overflow-hidden">
//           <p className="pb-3">
//             If you go over your organisations or user limit, a member of the team will reach out about bespoke pricing. In the meantime, our collaborative features won't appear in accounts or users that are over the 100-account or 1,000-user limit.
//           </p>
//         </div>
//        </div>


//         <button className=" col-start-1 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 flex flex-row justify-between items-center h-14" style={{ backgroundColor:"#F8F7F7"}}>
//           <p className=" ml-3">
//             What are the best tips for booking cheap flights?
//           </p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>
//         <button className="col-start-1 lg:col-start-7 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 bg-[#F8F7F7] flex flex-row justify-between items-center h-14">
//           <p className=" ml-3">How can I save money when i fly often?</p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>

//         <button className=" col-start-1 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 bg-[#F8F7F7] flex flex-row justify-between items-center h-14" >
//           <p className=" ml-3">How do I get the best airfare prices?</p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>
//         <button className="col-start-1 lg:col-start-7 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6 bg-[#F8F7F7] flex flex-row justify-between items-center h-14" >
//           <p className=" ml-3">Is booking a multi-flight cheaper?</p>
//           <ExpandMoreIcon className=" mr-3" />
//         </button>

        // <div className=" col-start-4 col-span-6 lg:col-start-6 md:col-start-5 lg:col-span-2 md:col-span-4 mt-2">
        //   <Button
        //     fullWidth
        //     className=" text-center bg-blue-900 normal-case hover:bg-blue-800 text-white h-12"
        //   >
        //     {" "}
        //     More FAQ
        //   </Button>
        // </div>
//       </div>
//     </Container>
//   );
// };

// export default Faq;
