import { Box, Container, Link, Typography } from "@mui/material";
import React from "react";
import Image from "next/image";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";

// next.config.js
// module.exports = {
//   logos: {
//     domains: ['www.worldholidayvibes.com'],
//   },
// };

const Footer = () => {
  const logos = [
    {
      src: "/Images/logos/ATOL_White.png",
    },
    {
      src: "/Images/logos/travel_aware_sherpa.svg",
    },
    {
      src: "/Images/logos/financial-protection.png",
    },
    {
      src: "/Images/logos/Flexible-payment-white.png",
    },
  ];

  return (
    <React.Fragment>
      <Box
        sx={{
          backgroundColor: "#013365",
          //height: "104px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          paddingY: "30px"
        }}
      >
        <Container>
          <div className=" flex justify-between items-center align-middle lg:flex-row flex-col gap-y-4">
            <div className="inline-block align-bottom">
              <Typography className=" text-white font-bold  sm:text-3xl text-xl ">
                Booking is safe with us! We are best indestry
              </Typography>
            </div>
            <div className=" flex gap-5 sm:flex-nowrap flex-wrap justify-center items-center">
              {logos.map((image, index) => (
                <Image
                  key={index}
                  width={80}
                  height={80}
                  src={image.src}
                  alt="logo"
                />
              ))}
            </div>
          </div>
        </Container>
      </Box>

      <Container>
        <div className=" mt-7 flex lg:justify-between lg:flex-row flex-col gap-y-4 lg:gap-x-6 md:justify-center">
          <div className=" ">
            <Typography variant="h5" className=" text-blue-950 font-semibold">
              Need Any Help
            </Typography>
            <div className=" border-b-2  border-blue-950 mt-5"></div>

            <div className=" border-l-4 border-blue-950 mt-5 h-fit  ">
              <Typography className=" ml-5 leading-10 font-semibold">
                Call 24/7 for any help
              </Typography>
              <Link
                variant="h5"
                underline="none"
                className=" hover:cursor-pointer hover:underline ml-5 text-black font-bold lg:text-2xl"
              >
                +4154 2231325651
              </Link>
            </div>

            <div className=" border-l-4 border-blue-950 mt-5 h-fit  ">
              <Typography className=" ml-5 leading-10 font-semibold">
                Mail to our support team
              </Typography>
              <Link
              
                underline="none"
                className="hover:cursor-pointer hover:underline ml-5 text-black font-bold lg:text-xl text-lg "
              >
                Support@flightvibes.com
              </Link>
            </div>

            <div className=" border-l-4 border-blue-950 mt-5 h-fit  ">
              <Typography className=" ml-5 leading-10 font-semibold">
                Contect us on
              </Typography>
              <div className=" flex gap-4 ml-5">
                <FacebookIcon />
                <InstagramIcon />
                <YouTubeIcon />
              </div>
            </div>
          </div>

          <div className=" ">
            <Typography variant="h5" className=" text-blue-950 font-semibold ">
              Use Full Links
            </Typography>
            <div className=" border-b-2  border-blue-950 mt-5"></div>
            <ul className=" flex lg:justify-between ">
              <div className=" flex flex-col gap-3 mt-3">
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Home
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Fligts
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Hotels
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Flash Deal
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Destination
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Promotion
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Help
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Sign in/Sign up
                  </Link>
                </li>
              </div>
              <div className=" flex flex-col gap-3 mt-3 ml-10">
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    About
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Testimonials
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Rewards
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Blog
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Meet the team
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Work with us
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    FAQ
                  </Link>
                </li>
                <li>
                  <Link
                    underline="none"
                    className=" hover:cursor-pointer hover:underline text-black font-semibold"
                  >
                    Privacy policy
                  </Link>
                </li>
              </div>
            </ul>
          </div>

          <div className=" flex-row">
            <div
              className=" h-44 rounded-lg pt-8 px-4 max-w-[27rem] min-w-[16rem]
            
                 bg-gradient-to-r 
                       from-teal-400 from-33.3%
                      via-blue-950 via-33.3% 
                      to-purple-600 to-33.3% 
                       bg-opacity-30"
            >
              <div className="flex items-center justify-between">
                <div className="">
                  <Typography variant="h5" className="  text-white ">
                    We accept
                  </Typography>
                </div>
                <div className=" bg-white h-px w-64  "></div>
              </div>
              <Image className=" mt-2"
                width={400}
                height={200}
                src="/Images/logos/payment-methods-we-accept.svg"
                alt="logo"
              />
            </div>
            <Typography
              variant="h5"
              className=" text-blue-950 font-semibold mt-4"
            >
              Need Any Help
            </Typography>
            <div className=" border-b-2  border-blue-950 mt-5"></div>
            <p className=" mt-2 font-semibold">
              For reservation and booking. Our team
              <br /> of experienced travel agents are 24/7 at your service to
              <br />
              help you.
            </p>
          </div>
        </div>

        <p className=" mt-8 text-gray-500  text-base text-left">
          Many of the flights and flight-inclusive holidays on lowcostvibes.com
          which depart from the UK are financially protected by the ATOL scheme.
          Please ask us to confirm what protection may apply to your booking. If
          you do not receive an ATOL Certificate then the booking will not be
          ATOL protected. If you do receive an ATOL Certificate but all the
          parts of your trip are not listed on it, those parts will not be ATOL
          protected. This website is intended primarily to residents of the UK.
          Please see our terms and conditions for information, or for more
          information about financial protection and the ATOL Certificate go to:
          www.atol.org.uk/ATOLCertificate
        </p>

        <div className=" border-b-2 border-blue-950 mt-5"></div>
        <div className=" flex lg:justify-between lg:flex-row flex-col mt-2">
          <Typography variant="subtitle1">Powered by Techneapp</Typography>

          <div>
            <Typography variant="subtitle1">
              {`@ ${new Date().getFullYear()} Vibes Group UK. All rights reserved. Tranding as flight vibes`}
            </Typography>
          </div>
        </div>
      </Container>
    </React.Fragment>
  );
};

export default Footer;
