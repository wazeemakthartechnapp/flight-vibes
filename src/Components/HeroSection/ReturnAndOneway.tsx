// components/ReturnButton.tsx
'use client'
import { useState } from 'react';
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

const ReturnButton = () => {
  const [buttonName, setButtonName] = useState('Return');
  const [showForm, setShowForm] = useState(false);

  const handleClick = () => {
    if (buttonName === 'Return') {
      setButtonName('One Way');
      setShowForm(false);
    } else {
      setButtonName('Return');
      setShowForm(true);
    }
  };

  return (
    <div>
      <button onClick={handleClick} className=" text-black font-bold flex gap-1">
        {buttonName}
        <KeyboardArrowDownIcon/>
      </button>
      {showForm && (
        <form className="mt-4">
          {/* Your 'Return' form elements go here */}
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="returnFormControlInput">
            Return Form Input
          </label>
          <input
            type="text"
            id="returnFormControlInput"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Return Input"
          />
        </form>
      )}
      {!showForm && (
        <form className="mt-4">
          {/* Your 'One Way' form elements go here */}
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="oneWayFormControlInput">
            One Way Form Input
          </label>
          <input
            type="text"
            id="oneWayFormControlInput"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="One Way Input"
          />
        </form>
      )}
    </div>
  );
};

export default ReturnButton;
