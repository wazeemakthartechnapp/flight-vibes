// components/BottomNavBar.tsx
"use client";
import React, { useEffect, useState } from "react";
import ScrollToBottom from "react-scroll-to-bottom";
import FlightIcon from "@mui/icons-material/Flight";
import HotelIcon from "@mui/icons-material/Hotel";
import BeachAccessOutlinedIcon from "@mui/icons-material/BeachAccessOutlined";

const BottomNav: React.FC = () => {
    const [isVisible, setIsVisible] = useState(true);

    useEffect(() => {
      const handleScroll = () => {
        const currentScrollPos = window.pageYOffset;
  
        if (currentScrollPos > 0 && isVisible) {
          setIsVisible(false);
        } else if (currentScrollPos === 0 || !isVisible) {
          setIsVisible(true);
        }
      };
  
      window.addEventListener('scroll', handleScroll);
  
      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }, [isVisible]);

  return (
<></>
  );
};

export default BottomNav;

