"use client";
import React, { useRef, useState, useEffect } from "react";
import FlightTakeoffIcon from "@mui/icons-material/FlightTakeoff";
import { CardContent, Typography } from "@mui/material";
import { BsFillTriangleFill } from "react-icons/bs";
import { GoDotFill } from "react-icons/go";
import Checkbox from "@mui/material/Checkbox";
import { RxDividerVertical } from "react-icons/rx";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import SearchIcon from "@mui/icons-material/Search";
import FlightLandIcon from "@mui/icons-material/FlightLand";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import "react-datepicker/dist/react-datepicker.css";
import { Controller, useForm } from "react-hook-form";
import { WiRainMix } from "react-icons/wi";
import { MdOutlineWbSunny } from "react-icons/md";
import { DateRange } from "react-date-range";
import "./YourComponent.css";
import ScrollToBottom from "react-scroll-to-bottom";
import FlightIcon from "@mui/icons-material/Flight";
import HotelIcon from "@mui/icons-material/Hotel";
import BeachAccessOutlinedIcon from "@mui/icons-material/BeachAccessOutlined";

const Searchformmob = () => {
  const {
    control,
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => {
    alert(JSON.stringify(data));
    // Displaying form data for demonstration purposes
    const formattedStartDate = dateRange[0].startDate.toLocaleDateString();
    const formattedEndDate = dateRange[0].endDate.toLocaleDateString();

    // Display the formatted dates in the alert
    alert(`Selected Date Range: ${formattedStartDate} to ${formattedEndDate}`);
  };

  const [dateRange, setDateRange] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);

  const [showCalendar, setShowCalendar] = useState(false);
  const dateRangeRef = useRef(null);

  const handleDateChange = (ranges: any) => {
    const { startDate, endDate } = ranges.selection;
    setDateRange([ranges.selection]);

    // Example: Log the start and end dates to the console
    console.log("Start Date:", startDate);
    console.log("End Date:", endDate);
  };

  const toggleCalendar = () => {
    setShowCalendar(!showCalendar);
  };

  const formatDay = (date: Date) => {
    return date.getDate();
  };

  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      setVisible(
        (prevScrollPos > currentScrollPos && prevScrollPos - currentScrollPos > 70) ||
        currentScrollPos < 200
      );
      setPrevScrollPos(currentScrollPos);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [prevScrollPos]);


  const [buttonColor1, setButtonColor1] = useState("bg-blue-950");
  const [buttonColor2, setButtonColor2] = useState("bg-slate-200");
  const [buttonColor3, setButtonColor3] = useState("bg-slate-200");
  const [iconcolor1, setIconColor1] = useState("text-white");
  const [iconcolor2, setIconColor2] = useState("text-black");
  const [iconcolor3, setIconColor3] = useState("text-black");
  const [showForm1, setShowForm1] = useState(true);
  const [showForm2, setShowForm2] = useState(false);
  const [showForm3, setShowForm3] = useState(false);

  const handleButtonClick = (buttonNumber: number): void => {
    switch (buttonNumber) {
      case 1:
        setButtonColor1("bg-blue-950");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-white");
        setIconColor2("text-black");
        setIconColor3("text-black");
        setShowForm1(true);
        setShowForm2(false);
        setShowForm3(false);
        break;
      case 2:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-blue-950");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-black");
        setIconColor2("text-white");
        setIconColor3("text-black");
        setShowForm1(false);
        setShowForm2(true);
        setShowForm3(false);
        break;
      case 3:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-blue-950");
        setIconColor1("text-black");
        setIconColor2("text-black");
        setIconColor3("text-white");
        setShowForm1(false);
        setShowForm2(false);
        setShowForm3(true);
        break;
      default:
        break;
    }
  };

  const customClickFunction = (): void => {
    // Add your custom click function for all buttons here
    console.log("Custom click function for all buttons!");
  };

  return (
    <React.Fragment>
      <ScrollToBottom>
        <div
          className={`${
            visible ? 'opacity-100' : 'opacity-0'
          } fixed bottom-0 left-0 z-50 w-full h-16 border-t border-gray-200 bg-[#E9EFFB] sm:hidden p-2 transition-transform duration-200 ease-in-out`}
        >
          {/* Your navigation bar content goes here */}
          <div className="grid gap-2 h-full max-w-lg grid-cols-3 mx-auto font-medium">
            <button
              type="button"
              className={`${buttonColor1} ${iconcolor1} inline-flex flex-col rounded-md items-center justify-center hover:bg-blue-950 hover:text-white group`}
              onClick={() => {
                handleButtonClick(1);
                customClickFunction();
              }}
            >
              <FlightIcon className={` ${iconcolor1} group-hover:fill-white`} />
              Flight
            </button>
            <button
              type="button"
              className={`${buttonColor2} ${iconcolor2} inline-flex flex-col rounded-md items-center justify-center hover:bg-blue-950 hover:text-white group`}
              onClick={() => {
                handleButtonClick(2);
                customClickFunction();
              }}
            >
              <HotelIcon className={` ${iconcolor2} group-hover:fill-white`} />
              Hotel
            </button>
            <button
              type="button"
              className={`${buttonColor3} ${iconcolor3} inline-flex flex-col rounded-md items-center justify-center hover:bg-blue-950 hover:text-white group`}
              onClick={() => {
                handleButtonClick(3);
                customClickFunction();
              }}
            >
              <BeachAccessOutlinedIcon
                className={` ${iconcolor3} group-hover:fill-white`}
              />
              Flight & Hotel
            </button>
          </div>
        </div>
      </ScrollToBottom>
      <div>
{/*-----------------------------------------------------------Flight Search Form----------------------------------------------------------*/}
        {showForm1 && (
          <div className="mt-0">
            <form className=" sm:hidden grid grid-cols-2 gap-1 p-1 bg-white shadow-lg h-fit w-full rounded-t-md mt-2">
              <div className=" col-span-2 flex justify-center items-center">
                <div className="">
                  <div className=" flex items-center ">
                    <div className=" flex justify-between">
                      <Typography>Return</Typography>
                      <KeyboardArrowDownIcon />
                    </div>
                    <div>
                      <RxDividerVertical size="25" />
                    </div>
                    <div className=" flex justify-between items-center ">
                      <Checkbox
                        size="small"
                        color="success"
                        defaultChecked
                        className=""
                      />
                      <Typography>Direct</Typography>
                    </div>
                  </div>
                </div>
              </div>

              <div className=" col-span-2 rounded-lg bg-[#E9EFFB] ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightTakeoffIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">From</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className=" flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-span-2 ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightLandIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">To</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="bg-[#E9EFFB] h-full p-2 rounded-lg"
              >
                <div className=" ">
                  <div className="flex flex-col gap-1">
                    <div className="flex justify-between items-center">
                      <p className="text-sm text-gray-500 flex items-center">
                        Depart on
                      </p>

                      <div>
                        <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                      </div>
                    </div>
                    <p className="text-4xl text-[#013365] font-semibold">
                      {formatDay(dateRange[0].startDate)}
                    </p>

                    <p className="text-md text-gray-500">
                      {dateRange[0].startDate.toDateString()}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <div>
                      <WiRainMix className="text-5xl text-blue-900 bg-none" />
                    </div>
                    <div className="flex flex-col md:mt-4">
                      <p className="text-xl">29.6 C</p>
                      <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                      >
                        Moderate Rain
                      </Typography>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="p-2 rounded-lg bg-[#E9EFFB] h-full "
              >
                <div className="flex flex-col gap-1">
                  <div className=" flex justify-between items-center">
                    <p className=" text-sm text-gray-500 flex items-cente">
                      Return On
                    </p>

                    <div className=" gap-2 flex">
                      <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                      <GoDotFill
                        fontSize="medium"
                        className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                      />
                    </div>
                  </div>
                  <p className="text-4xl text-[#013365] font-semibold">
                    {formatDay(dateRange[0].endDate)}
                  </p>

                  <p {...register("From")} className="text-md text-gray-500">
                    {dateRange[0].endDate.toDateString()}
                  </p>
                </div>
                <div className=" flex gap-1 justify-between items-center md:mt-4">
                  <div>
                    <MdOutlineWbSunny className="text-5xl text-orange-500" />
                  </div>
                  <div>
                    <p className="text-xl">29.6 C</p>
                    <Typography
                      sx={{ fontSize: 14 }}
                      color="text.secondary"
                      gutterBottom
                    >
                      Moderate Rain
                    </Typography>
                  </div>
                </div>
              </div>

              <div className=" col-span-2 ">
                <div className="flex justify-between px-3 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                  <div className="flex flex-col gap-1 flex-grow">
                    <p className="text-sm text-gray-500">Travelers & class</p>
                    <div className=" relative z-0 w-full mb-4 group">
                      <input
                        type="text"
                        name="floting_Destination_Airport"
                        id="floting_Destination_Airport"
                        className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                        placeholder=" "
                        required
                      />
                      <label
                        htmlFor="floting_Destination_Airport"
                        className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        Destination / Airport
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <button className=" col-span-2 bg-blue-950 rounded-3xl text-white py-1">
                <SearchIcon fontSize="large" />
                Search
              </button>
              {showCalendar && (
                <div className="modal-overlay">
                  <div className="modal-content bg-white rounded shadow-md flex flex-col justify-end w-full">
                    <Controller
                      control={control}
                      name="departDate"
                      render={({ field }) => (
                        <DateRange
                          ranges={dateRange}
                          onChange={handleDateChange}
                          moveRangeOnFirstSelection={false}
                          editableDateInputs={true}
                          className=" w-full "
                        />
                      )}
                    />
                    <button
                      onClick={toggleCalendar}
                      className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                    >
                      Apply Date
                    </button>
                  </div>
                </div>
              )}
            </form>
          </div>
        )}

{/*-----------------------------------------------------------Hotel Search Form----------------------------------------------------------*/}
        {showForm2 && (
          <div className="mt-0">
            <form className=" sm:hidden grid grid-cols-2 gap-1 p-1 bg-white shadow-lg h-fit w-full rounded-t-md mt-2">
              <div className=" col-span-2 rounded-lg bg-[#E9EFFB] ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightTakeoffIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">From</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className=" flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-span-2 ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightLandIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">To</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="bg-[#E9EFFB] h-full p-2 rounded-lg"
              >
                <div className=" ">
                  <div className="flex flex-col gap-1">
                    <div className="flex justify-between items-center">
                      <p className="text-sm text-gray-500 flex items-center">
                        Check in
                      </p>

                      <div>
                        <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                      </div>
                    </div>
                    <p className="text-4xl text-[#013365] font-semibold">
                      {formatDay(dateRange[0].startDate)}
                    </p>

                    <p className="text-md text-gray-500">
                      {dateRange[0].startDate.toDateString()}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <div>
                      <WiRainMix className="text-5xl text-blue-900 bg-none" />
                    </div>
                    <div className="flex flex-col md:mt-4">
                      <p className="text-xl">29.6 C</p>
                      <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                      >
                        Moderate Rain
                      </Typography>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="p-2 rounded-lg bg-[#E9EFFB] h-full "
              >
                <div className="flex flex-col gap-1">
                  <div className=" flex justify-between items-center">
                    <p className=" text-sm text-gray-500 flex items-cente">
                      Check out
                    </p>

                    <div className=" gap-2 flex">
                      <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                      <GoDotFill
                        fontSize="medium"
                        className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                      />
                    </div>
                  </div>
                  <p className="text-4xl text-[#013365] font-semibold">
                    {formatDay(dateRange[0].endDate)}
                  </p>

                  <p {...register("From")} className="text-md text-gray-500">
                    {dateRange[0].endDate.toDateString()}
                  </p>
                </div>
                <div className=" flex gap-1 justify-between items-center md:mt-4">
                  <div>
                    <MdOutlineWbSunny className="text-5xl text-orange-500" />
                  </div>
                  <div>
                    <p className="text-xl">29.6 C</p>
                    <Typography
                      sx={{ fontSize: 14 }}
                      color="text.secondary"
                      gutterBottom
                    >
                      Moderate Rain
                    </Typography>
                  </div>
                </div>
              </div>

              <div className=" col-span-2 ">
                <div className="flex justify-between px-3 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                  <div className="flex flex-col gap-1 flex-grow">
                    <p className="text-sm text-gray-500">Travelers & class</p>
                    <div className=" relative z-0 w-full mb-4 group">
                      <input
                        type="text"
                        name="floting_Destination_Airport"
                        id="floting_Destination_Airport"
                        className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                        placeholder=" "
                        required
                      />
                      <label
                        htmlFor="floting_Destination_Airport"
                        className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        Destination / Airport
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <button className=" col-span-2 bg-blue-950 rounded-3xl text-white py-1">
                <SearchIcon fontSize="large" />
                Search
              </button>
              {showCalendar && (
                <div className="modal-overlay">
                  <div className="modal-content bg-white rounded shadow-md flex flex-col justify-end w-full">
                    <Controller
                      control={control}
                      name="departDate"
                      render={({ field }) => (
                        <DateRange
                          ranges={dateRange}
                          onChange={handleDateChange}
                          moveRangeOnFirstSelection={false}
                          editableDateInputs={true}
                          className=" w-full "
                        />
                      )}
                    />
                    <button
                      onClick={toggleCalendar}
                      className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                    >
                      Apply Date
                    </button>
                  </div>
                </div>
              )}
            </form>
          </div>
        )}
{/*-----------------------------------------------------------Flight and Hotel Search Form----------------------------------------------------------*/}
        {showForm3 && (
          <div className="mt-0">
            <form className=" sm:hidden grid grid-cols-2 gap-1 p-1 bg-white shadow-lg h-fit w-full rounded-t-md mt-2">
              <div className=" col-span-2 flex justify-center items-center">
                <div className="">
                  <div className=" flex items-center ">
                    <div className=" flex justify-between">
                      <Typography>Return</Typography>
                      <KeyboardArrowDownIcon />
                    </div>
                    <div>
                      <RxDividerVertical size="25" />
                    </div>
                    <div className=" flex justify-between items-center ">
                      <Checkbox
                        size="small"
                        color="success"
                        defaultChecked
                        className=""
                      />
                      <Typography>Direct</Typography>
                    </div>
                  </div>
                </div>
              </div>

              <div className=" col-span-2 rounded-lg bg-[#E9EFFB] ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightTakeoffIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">From</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className=" flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-span-2 ">
                <div className="px-3 py-1 items-center rounded-lg bg-[#E9EFFB] h-full">
                  <div className=" flex gap-4 items-center">
                    <div className=" h-full px-3 py-5 bg-white rounded-md text-center ">
                      <FlightLandIcon className="object-right" />
                    </div>
                    <div className="flex flex-col gap-1 flex-grow">
                      <p className="text-sm text-gray-500">To</p>
                      <div className=" relative z-0 w-full mb-4 group">
                        <input
                          type="text"
                          name="floting_Destination_Airport"
                          id="floting_Destination_Airport"
                          className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                          placeholder=" "
                          required
                        />
                        <label
                          htmlFor="floting_Destination_Airport"
                          className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Destination / Airport
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="bg-[#E9EFFB] h-full p-2 rounded-lg"
              >
                <div className=" ">
                  <div className="flex flex-col gap-1">
                    <div className="flex justify-between items-center">
                      <p className="text-sm text-gray-500 flex items-center">
                        Depart on
                      </p>

                      <div>
                        <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                      </div>
                    </div>
                    <p className="text-4xl text-[#013365] font-semibold">
                      {formatDay(dateRange[0].startDate)}
                    </p>

                    <p className="text-md text-gray-500">
                      {dateRange[0].startDate.toDateString()}
                    </p>
                  </div>
                  <div className="flex items-center justify-between">
                    <div>
                      <WiRainMix className="text-5xl text-blue-900 bg-none" />
                    </div>
                    <div className="flex flex-col md:mt-4">
                      <p className="text-xl">29.6 C</p>
                      <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                      >
                        Moderate Rain
                      </Typography>
                    </div>
                  </div>
                </div>
              </div>

              <div
                onClick={toggleCalendar}
                className="p-2 rounded-lg bg-[#E9EFFB] h-full "
              >
                <div className="flex flex-col gap-1">
                  <div className=" flex justify-between items-center">
                    <p className=" text-sm text-gray-500 flex items-cente">
                      Return On
                    </p>

                    <div className=" gap-2 flex">
                      <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                      <GoDotFill
                        fontSize="medium"
                        className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                      />
                    </div>
                  </div>
                  <p className="text-4xl text-[#013365] font-semibold">
                    {formatDay(dateRange[0].endDate)}
                  </p>

                  <p {...register("From")} className="text-md text-gray-500">
                    {dateRange[0].endDate.toDateString()}
                  </p>
                </div>
                <div className=" flex gap-1 justify-between items-center md:mt-4">
                  <div>
                    <MdOutlineWbSunny className="text-5xl text-orange-500" />
                  </div>
                  <div>
                    <p className="text-xl">29.6 C</p>
                    <Typography
                      sx={{ fontSize: 14 }}
                      color="text.secondary"
                      gutterBottom
                    >
                      Moderate Rain
                    </Typography>
                  </div>
                </div>
              </div>

              <div className=" col-span-2 ">
                <div className="flex justify-between px-3 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                  <div className="flex flex-col gap-1 flex-grow">
                    <p className="text-sm text-gray-500">Travelers & class</p>
                    <div className=" relative z-0 w-full mb-4 group">
                      <input
                        type="text"
                        name="floting_Destination_Airport"
                        id="floting_Destination_Airport"
                        className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                        placeholder=" "
                        required
                      />
                      <label
                        htmlFor="floting_Destination_Airport"
                        className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        Destination / Airport
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <button className=" col-span-2 bg-blue-950 rounded-3xl text-white py-1">
                <SearchIcon fontSize="large" />
                Search
              </button>
              {showCalendar && (
                <div className="modal-overlay">
                  <div className="modal-content bg-white rounded shadow-md flex flex-col justify-end w-full">
                    <Controller
                      control={control}
                      name="departDate"
                      render={({ field }) => (
                        <DateRange
                          ranges={dateRange}
                          onChange={handleDateChange}
                          moveRangeOnFirstSelection={false}
                          editableDateInputs={true}
                          className=" w-full "
                        />
                      )}
                    />
                    <button
                      onClick={toggleCalendar}
                      className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                    >
                      Apply Date
                    </button>
                  </div>
                </div>
              )}
            </form>
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default Searchformmob;
