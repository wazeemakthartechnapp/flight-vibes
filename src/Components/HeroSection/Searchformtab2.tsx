"use client";
import React, { useRef, useState } from "react";
import FlightTakeoffIcon from "@mui/icons-material/FlightTakeoff";
import { CardContent, Container, Typography } from "@mui/material";
import { BsFillTriangleFill } from "react-icons/bs";
import { GoDotFill } from "react-icons/go";
import Checkbox from "@mui/material/Checkbox";
import { RxDividerVertical } from "react-icons/rx";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import SearchIcon from "@mui/icons-material/Search";
import FlightLandIcon from "@mui/icons-material/FlightLand";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import "react-datepicker/dist/react-datepicker.css";
import { Controller, useForm } from "react-hook-form";
import { WiRainMix } from "react-icons/wi";
import { MdOutlineWbSunny } from "react-icons/md";
import { DateRangePicker } from "react-date-range";
import Searchformmob from "./Searchformmob";
import FlightIcon from "@mui/icons-material/Flight";
import HotelIcon from "@mui/icons-material/Hotel";
import BeachAccessOutlinedIcon from "@mui/icons-material/BeachAccessOutlined";
import "./YourComponent.css";
import OriginalComponentFlight from "./Searchform/flight/ForFlight/YourComponent";
import OriginalComponentHotel from "./Searchform/Hotel/forHotel/YourComponent";
import OriginalComponent from "./Searchform/FligtAndHotel/ForFlightAndHotel/YourComponent";

const Searchformtab2: React.FC = () => {
  const {
    control,
    handleSubmit,
    setValue,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => {
    alert(JSON.stringify(data));
    // Displaying form data for demonstration purposes
    const formattedStartDate = dateRange[0].startDate.toLocaleDateString();
    const formattedEndDate = dateRange[0].endDate.toLocaleDateString();

    // Display the formatted dates in the alert
    alert(`Selected Date Range: ${formattedStartDate} to ${formattedEndDate}`);
  };

  const [dateRange, setDateRange] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);

  const [showCalendar, setShowCalendar] = useState(false);
  const dateRangeRef = useRef(null);

  const handleDateChange = (ranges: any) => {
    const { startDate, endDate } = ranges.selection;
    setDateRange([ranges.selection]);

    // Example: Log the start and end dates to the console
    console.log("Start Date:", startDate);
    console.log("End Date:", endDate);
  };

  const toggleCalendar = () => {
    setShowCalendar(!showCalendar);
  };

  const formatDay = (date: Date) => {
    return date.getDate();
  };

  const [buttonColor1, setButtonColor1] = useState("bg-blue-950");
  const [buttonColor2, setButtonColor2] = useState("bg-slate-200");
  const [buttonColor3, setButtonColor3] = useState("bg-slate-200");
  const [iconcolor1, setIconColor1] = useState("text-white");
  const [iconcolor2, setIconColor2] = useState("text-black");
  const [iconcolor3, setIconColor3] = useState("text-black");
  const [showForm1, setShowForm1] = useState(true);
  const [showForm2, setShowForm2] = useState(false);
  const [showForm3, setShowForm3] = useState(false);

  const handleButtonClick = (buttonNumber: number): void => {
    switch (buttonNumber) {
      case 1:
        setButtonColor1("bg-blue-950");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-white");
        setIconColor2("text-black");
        setIconColor3("text-black");
        setShowForm1(true);
        setShowForm2(false);
        setShowForm3(false);
        break;
      case 2:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-blue-950");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-black");
        setIconColor2("text-white");
        setIconColor3("text-black");
        setShowForm1(false);
        setShowForm2(true);
        setShowForm3(false);
        break;
      case 3:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-blue-950");
        setIconColor1("text-black");
        setIconColor2("text-black");
        setIconColor3("text-white");
        setShowForm1(false);
        setShowForm2(false);
        setShowForm3(true);
        break;
      default:
        break;
    }
  };

  const customClickFunction = (): void => {
    // Add your custom click function for all buttons here
    console.log("Custom click function for all buttons!");
  };

  return (
    <React.Fragment>
      <Container className="">
        <div className=" sm:flex justify-end  md:hidden hidden">
          <div className="space-x-4 flex left-0 flex-row bg-white rounded-md p-2 h-fit w-fit justify-end">
            <button
              type="button"
              className={` ${buttonColor1} ${iconcolor1} uppercase  p-2 hover:text-white  hover:bg-blue-950 h-20 w-24 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
              onClick={() => {
                handleButtonClick(1);
                customClickFunction();
              }}
            >
              <FlightIcon className={` group-hover:fill-white ${iconcolor1}`} />{" "}
              Flight
            </button>
            <button
              type="button"
              className={` ${buttonColor2} ${iconcolor2} uppercase  p-2 hover:text-white hover:bg-blue-950 h-20 w-24 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
              onClick={() => {
                handleButtonClick(2);
                customClickFunction();
              }}
            >
              <HotelIcon className={` group-hover:fill-white ${iconcolor2}`} />{" "}
              Hotel
            </button>
            <button
              type="button"
              className={` ${buttonColor3} ${iconcolor3} uppercase  p-2 hover:text-white hover:bg-blue-950 h-20 w-40 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
              onClick={() => {
                handleButtonClick(3);
                customClickFunction();
              }}
            >
              <BeachAccessOutlinedIcon
                className={` group-hover:fill-white ${iconcolor3}`}
              />{" "}
              Flight & Hotel
            </button>
          </div>
        </div>

        {showForm1 && (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="sm:grid relative md:hidden hidden  sm:grid-cols-12 gap-2 w-full bg-white rounded-lg shadow-lg h-fit p-2"
          >
            <div className=" col-span-12 flex justify-center items-center">
              <div className="">
                <div className=" flex items-center ">
                  <div className=" flex justify-between">
                    <Typography>Return</Typography>
                    <KeyboardArrowDownIcon />
                  </div>
                  <div>
                    <RxDividerVertical size="25" />
                  </div>
                  <div className=" flex justify-between items-center ">
                    <Checkbox
                      size="small"
                      color="success"
                      defaultChecked
                      className=""
                    />
                    <Typography>Direct</Typography>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-span-6">
              <div className="flex justify-between pl-4 pr-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">From</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightTakeoffIcon className="object-right" />
              </div>
            </div>

            <button className=" absolute rounded-full px-3 py-3 bg-[#00C590] mt-[5rem] ml-[20.5rem] rotate-90">
              <SwapVertIcon className=" fill-white" />
            </button>

            <div className=" col-span-6">
              <div className="flex justify-between pr-4 pl-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">To</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightLandIcon className="object-right" />
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 rounded-lg bg-[#E9EFFB] p-2 flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className="flex justify-between items-center">
                  <p className="text-sm text-gray-500 flex items-center">
                    Depart on
                  </p>

                  <div>
                    <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].startDate)}
                </p>

                <p className="text-md text-gray-500">
                  {dateRange[0].startDate.toDateString()}
                </p>
              </div>
              <div className="flex items-center justify-between">
                <div>
                  <WiRainMix className="text-5xl text-blue-900 bg-none" />
                </div>
                <div className="flex flex-col md:mt-4">
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 p-2 rounded-lg bg-[#E9EFFB] flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className=" flex justify-between items-center">
                  <p className=" text-sm text-gray-500 flex items-cente">
                    Return On
                  </p>

                  <div className=" gap-2 flex">
                    <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                    <GoDotFill
                      fontSize="medium"
                      className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                    />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].endDate)}
                </p>

                <p {...register("From")} className="text-md text-gray-500">
                  {dateRange[0].endDate.toDateString()}
                </p>
              </div>
              <div className=" flex gap-1 justify-between items-center md:mt-4">
                <div>
                  <MdOutlineWbSunny className="text-5xl text-orange-500" />
                </div>
                <div>
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div className="col-span-6">
              <OriginalComponentFlight />
            </div>

            <div className=" col-span-6 col-start-7 rounded-lg bg-blue-900 flex justify-center items-center ">
              <button className="flex justify-center items-center text-white py-3 w-full gap-3">
                <SearchIcon fontSize="large" />
                <Typography variant="h5">Search</Typography>
              </button>
            </div>
            {showCalendar && (
              <div className="modal-overlay">
                <div className="modal-content bg-white p-8 rounded shadow-md flex flex-col justify-end">
                  <Controller
                    control={control}
                    name="departDate"
                    render={({ field }) => (
                      <DateRangePicker
                        ranges={dateRange}
                        onChange={handleDateChange}
                        editableDateInputs
                      />
                    )}
                  />
                  <button
                    onClick={toggleCalendar}
                    className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                  >
                    Apply Date
                  </button>
                </div>
              </div>
            )}
          </form>
        )}

        {showForm2 && (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="sm:grid relative md:hidden hidden  sm:grid-cols-12 gap-2 w-full bg-white rounded-lg shadow-lg h-fit p-2"
          >
            <div className="col-span-6">
              <div className="flex justify-between pl-4 pr-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">From</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightTakeoffIcon className="object-right" />
              </div>
            </div>

            <button className=" absolute rounded-full px-3 py-3 bg-[#00C590] mt-[2rem] ml-[20.5rem] rotate-90">
              <SwapVertIcon className=" fill-white" />
            </button>

            <div className=" col-span-6">
              <div className="flex justify-between pr-4 pl-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">To</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightLandIcon className="object-right" />
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 rounded-lg bg-[#E9EFFB] p-2 flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className="flex justify-between items-center">
                  <p className="text-sm text-gray-500 flex items-center">
                    Depart on
                  </p>

                  <div>
                    <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].startDate)}
                </p>

                <p className="text-md text-gray-500">
                  {dateRange[0].startDate.toDateString()}
                </p>
              </div>
              <div className="flex items-center justify-between">
                <div>
                  <WiRainMix className="text-5xl text-blue-900 bg-none" />
                </div>
                <div className="flex flex-col md:mt-4">
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 p-2 rounded-lg bg-[#E9EFFB] flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className=" flex justify-between items-center">
                  <p className=" text-sm text-gray-500 flex items-cente">
                    Return On
                  </p>

                  <div className=" gap-2 flex">
                    <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                    <GoDotFill
                      fontSize="medium"
                      className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                    />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].endDate)}
                </p>

                <p {...register("From")} className="text-md text-gray-500">
                  {dateRange[0].endDate.toDateString()}
                </p>
              </div>
              <div className=" flex gap-1 justify-between items-center md:mt-4">
                <div>
                  <MdOutlineWbSunny className="text-5xl text-orange-500" />
                </div>
                <div>
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div className="col-span-6">
              <OriginalComponentHotel />
            </div>

            <div className=" col-span-6 col-start-7 rounded-lg bg-blue-900 flex justify-center items-center ">
              <button className="flex justify-center items-center text-white py-3 w-full gap-3">
                <SearchIcon fontSize="large" />
                <Typography variant="h5">Search</Typography>
              </button>
            </div>
            {showCalendar && (
              <div className="modal-overlay">
                <div className="modal-content bg-white p-8 rounded shadow-md flex flex-col justify-end">
                  <Controller
                    control={control}
                    name="departDate"
                    render={({ field }) => (
                      <DateRangePicker
                        ranges={dateRange}
                        onChange={handleDateChange}
                        editableDateInputs
                      />
                    )}
                  />
                  <button
                    onClick={toggleCalendar}
                    className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                  >
                    Apply Date
                  </button>
                </div>
              </div>
            )}
          </form>
        )}

        {showForm3 && (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="sm:grid relative md:hidden hidden  sm:grid-cols-12 gap-2 w-full bg-white rounded-lg shadow-lg h-fit p-2"
          >
            <div className=" col-span-6 flex justify-center items-center">
              <div className="">
                <div className=" flex items-center ">
                  <div className=" flex justify-between">
                    <Typography>Return</Typography>
                    <KeyboardArrowDownIcon />
                  </div>
                  <div>
                    <RxDividerVertical size="25" />
                  </div>
                  <div className=" flex justify-between items-center ">
                    <Checkbox
                      size="small"
                      color="success"
                      defaultChecked
                      className=""
                    />
                    <Typography>Direct</Typography>
                  </div>
                </div>
              </div>
            </div>

            <div className=" col-span-6"></div>

            <div className="col-span-6">
              <div className="flex justify-between pl-4 pr-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">From</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightTakeoffIcon className="object-right" />
              </div>
            </div>

            <button className=" absolute rounded-full px-3 py-3 bg-[#00C590] mt-[5rem] ml-[20.5rem] rotate-90">
              <SwapVertIcon className=" fill-white" />
            </button>

            <div className=" col-span-6">
              <div className="flex justify-between pr-4 pl-7 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
                <div className="flex flex-col gap-1 flex-grow">
                  <p className="text-sm text-gray-500">To</p>
                  <div className=" relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="floting_Destination_Airport"
                      id="floting_Destination_Airport"
                      className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                    />
                    <label
                      htmlFor="floting_Destination_Airport"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Destination / Airport
                    </label>
                  </div>
                </div>
                <FlightLandIcon className="object-right" />
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 rounded-lg bg-[#E9EFFB] p-2 flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className="flex justify-between items-center">
                  <p className="text-sm text-gray-500 flex items-center">
                    Depart on
                  </p>

                  <div>
                    <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].startDate)}
                </p>

                <p className="text-md text-gray-500">
                  {dateRange[0].startDate.toDateString()}
                </p>
              </div>
              <div className="flex items-center justify-between">
                <div>
                  <WiRainMix className="text-5xl text-blue-900 bg-none" />
                </div>
                <div className="flex flex-col md:mt-4">
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div
              onClick={toggleCalendar}
              className=" col-span-3 row-span-2 p-2 rounded-lg bg-[#E9EFFB] flex flex-col justify-between"
            >
              <div className="flex flex-col gap-1">
                <div className=" flex justify-between items-center">
                  <p className=" text-sm text-gray-500 flex items-cente">
                    Return On
                  </p>

                  <div className=" gap-2 flex">
                    <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                    <GoDotFill
                      fontSize="medium"
                      className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                    />
                  </div>
                </div>
                <p className="text-6xl text-[#013365] font-semibold">
                  {formatDay(dateRange[0].endDate)}
                </p>

                <p {...register("From")} className="text-md text-gray-500">
                  {dateRange[0].endDate.toDateString()}
                </p>
              </div>
              <div className=" flex gap-1 justify-between items-center md:mt-4">
                <div>
                  <MdOutlineWbSunny className="text-5xl text-orange-500" />
                </div>
                <div>
                  <p className="text-xl">29.6 C</p>
                  <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Moderate Rain
                  </Typography>
                </div>
              </div>
            </div>

            <div className="col-span-6">
              <OriginalComponent />
            </div>

            <div className=" col-span-6 col-start-7 rounded-lg bg-blue-900 flex justify-center items-center ">
              <button className="flex justify-center items-center text-white py-3 w-full gap-3">
                <SearchIcon fontSize="large" />
                <Typography variant="h5">Search</Typography>
              </button>
            </div>
            {showCalendar && (
              <div className="modal-overlay">
                <div className="modal-content bg-white p-8 rounded shadow-md flex flex-col justify-end">
                  <Controller
                    control={control}
                    name="departDate"
                    render={({ field }) => (
                      <DateRangePicker
                        ranges={dateRange}
                        onChange={handleDateChange}
                        editableDateInputs
                      />
                    )}
                  />
                  <button
                    onClick={toggleCalendar}
                    className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
                  >
                    Apply Date
                  </button>
                </div>
              </div>
            )}
          </form>
        )}
      </Container>

      <Searchformmob />
    </React.Fragment>
  );
};

export default Searchformtab2;
