"use client";
import { useState } from "react";
import FlightIcon from "@mui/icons-material/Flight";
import HotelIcon from "@mui/icons-material/Hotel";
import BeachAccessOutlinedIcon from "@mui/icons-material/BeachAccessOutlined";
import { Container } from "@mui/material";
import Flight from "./flight/Flight";
import Hotel from "./Hotel/Hotel";
import FlightAndHotel from "./FligtAndHotel/FlightAndHotel";


const ButtonPage: React.FC = () => {
  const [buttonColor1, setButtonColor1] = useState("bg-blue-950");
  const [buttonColor2, setButtonColor2] = useState("bg-slate-200");
  const [buttonColor3, setButtonColor3] = useState("bg-slate-200");
  const [iconcolor1, setIconColor1] = useState("text-white");
  const [iconcolor2, setIconColor2] = useState("text-black");
  const [iconcolor3, setIconColor3] = useState("text-black");
  const [showForm1, setShowForm1] = useState(true);
  const [showForm2, setShowForm2] = useState(false);
  const [showForm3, setShowForm3] = useState(false);

  const handleButtonClick = (buttonNumber: number): void => {
    switch (buttonNumber) {
      case 1:
        setButtonColor1("bg-blue-950");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-white");
        setIconColor2("text-black");
        setIconColor3("text-black");
        setShowForm1(true);
        setShowForm2(false);
        setShowForm3(false);
        break;
      case 2:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-blue-950");
        setButtonColor3("bg-slate-200");
        setIconColor1("text-black");
        setIconColor2("text-white");
        setIconColor3("text-black");
        setShowForm1(false);
        setShowForm2(true);
        setShowForm3(false);
        break;
      case 3:
        setButtonColor1("bg-slate-200");
        setButtonColor2("bg-slate-200");
        setButtonColor3("bg-blue-950");
        setIconColor1("text-black");
        setIconColor2("text-black");
        setIconColor3("text-white");
        setShowForm1(false);
        setShowForm2(false);
        setShowForm3(true);
        break;
      default:
        break;
    }
  };

  const customClickFunction = (): void => {
    // Add your custom click function for all buttons here
    console.log("Custom click function for all buttons!");
  };

  return (
    <Container>
    <div className="flex flex-col">
      <div className=" flex justify-between items-end">
        <div className=" flex lg:flex-row flex-col justify-between lg:mt-24 gap-12 ">
          <h1 className=" text-white font-semibold sm:text-5xl text-2xl justify-center md:items-center text-left  md:mr-24 mb-4">
            Exploring the World
            <br /> in Comfort. Enjoy Your Life
          </h1>
        </div>
        <div className="space-x-4 md:flex hidden flex-row items-end bg-white rounded-md p-2 h-fit justify-end">
          <button
            type="button"
            className={` ${buttonColor1} ${iconcolor1} uppercase  p-2 hover:text-white  hover:bg-blue-950 h-20 w-24 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
            onClick={() => {
              handleButtonClick(1);
              customClickFunction();
            }}
          >
            <FlightIcon className={` group-hover:fill-white ${iconcolor1}`} />{" "}
            Flight
          </button>
          <button
            type="button"
            className={` ${buttonColor2} ${iconcolor2} uppercase  p-2 hover:text-white hover:bg-blue-950 h-20 w-24 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
            onClick={() => {
              handleButtonClick(2);
              customClickFunction();
            }}
          >
            <HotelIcon className={` group-hover:fill-white ${iconcolor2}`} />{" "}
            Hotel
          </button>
          <button
            type="button"
            className={` ${buttonColor3} ${iconcolor3} uppercase  p-2 hover:text-white hover:bg-blue-950 h-20 w-40 group text-center flex gap-2 rounded-md flex-col justify-center items-center transition-all`}
            onClick={() => {
              handleButtonClick(3);
              customClickFunction();
            }}
          >
            <BeachAccessOutlinedIcon
              className={` group-hover:fill-white ${iconcolor3}`}
            />{" "}
            Flight & Hotel
          </button>
        </div>
      </div>

      {showForm1 && (
        <div className="mt-0">
          <Flight/>
        </div>
      )}

      {showForm2 && (
        <div className="mt-0">
          <Hotel/>
        </div>
      )}

      {showForm3 && (
        <div className="mt-0">
         <FlightAndHotel/>
        </div>
      )}
    </div>
    </Container>
  );
};

export default ButtonPage;
