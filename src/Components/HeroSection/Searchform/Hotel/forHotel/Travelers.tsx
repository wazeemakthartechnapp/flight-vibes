// TravelersAndClassModal.tsx
"use client";
import React, { useState, useEffect, useRef } from "react";
import { RiCloseFill } from "react-icons/ri";

interface TravelersAndClassFormProps {
  formData: {
    travelers: string;
    room: string;
    travelClass: string;
    adults: string;
    children: string;
  };
  onChange: (data: { [key: string]: string }) => void;
  onClose: () => void;
}

interface Room {
  adults: string;
  children: string;
}

const TravelersAndClassForm: React.FC<TravelersAndClassFormProps> = ({
  formData,
  onChange,
  onClose,
}) => {
  const formRef = useRef<HTMLDivElement>(null);

  const maxRooms = 3;
  const [rooms, setRooms] = useState<Room[]>([{ adults: "0", children: "0" }]);

  const handleAddRoom = () => {
    if (rooms.length < maxRooms) {
      setRooms([...rooms, { adults: "0", children: "0" }]);
    }
  };

  const handleRemoveRoom = () => {
    if (rooms.length > 1) {
      setRooms(rooms.slice(0, -1));
    }
  };

  const handleChange = (index: number, field: keyof Room, value: string) => {
    const updatedRooms = [...rooms];
    updatedRooms[index][field] = value;
    setRooms(updatedRooms);
  };

  const handleIncrement = (field: keyof Room, index: number) => {
    const currentValue = parseInt(rooms[index][field], 10) || 0;
    const updatedRooms = [...rooms];
    updatedRooms[index][field] = (currentValue + 1).toString();
    setRooms(updatedRooms);
  };

  const handleDecrement = (field: keyof Room, index: number) => {
    const currentValue = parseInt(rooms[index][field], 10) || 0;
    if (currentValue > 0) {
      const updatedRooms = [...rooms];
      updatedRooms[index][field] = (currentValue - 1).toString();
      setRooms(updatedRooms);
    }
  };

  const calculateTotalTravelers = (): string => {
    const adults = parseInt(formData.adults, 10) || 0;
    const children = parseInt(formData.children, 10) || 0;
    return (adults + children).toString();
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // Handle form submission logic here
    console.log("Form submitted:", formData);
    onClose(); // Close the form after submission
  };

  // Add event listener to close the form when clicking outside of it
  useEffect(() => {
    const handleOutsideClick = (e: MouseEvent) => {
      if (formRef.current && !formRef.current.contains(e.target as Node)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  const handleCancel = () => {
    // Reset the form data or perform any other cancel logic
    onClose();
  };

  const handleApply = () => {
    // You can perform any additional logic before closing the form
    onClose();
  };

  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div ref={formRef} className="bg-white p-5 rounded-lg">
        <div className="flex justify-between mb-4">
          <div>
            <p className=" text-xl font-bold">Travellers & Rooms</p>
            <p className=" text-sm text-gray-600">
              Select travellers age and hotel rooms count
            </p>
          </div>

          <button
            type="button"
            onClick={onClose}
            className="px-4 py-2 bg-gray-500 text-white rounded-md"
          >
            <RiCloseFill />
          </button>
        </div>
        <div className=" min-h-fit max-h-72 overflow-y-scroll">
          {rooms.map((room, index) => (
            <div key={index}>
              <div className=" flex justify-between">
                <h2 className="text-xl font-bold mb-2">Room {index + 1}</h2>
                <button
                  type="button"
                  onClick={handleRemoveRoom}
                  className=" uppercase text-base font-semibold text-blue-600"
                >
                  Remove Room
                </button>
              </div>

              <div className=" flex gap-4 justify-around mb-3">
                <div className=" flex flex-col justify-start border-2 p-2 rounded-md bg-[#E9EFFB]">
                  <label
                    className="block text-gray-700 text-base font-semibold mb-2 border-b-2  border-black pb-1"
                    htmlFor="travelers"
                  >
                    Adults
                    <p className=" text-xs text-gray-600">Ages 18 or above</p>
                  </label>
                  <div className="flex items-center">
                    <button
                      type="button"
                      onClick={() => handleDecrement("adults", index)}
                      className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                    >
                      -
                    </button>
                    {/* <input
                    type="text"
                    inputMode="numeric" // Set input mode to numeric
                    pattern="[0-10]*" // Allow only numeric input
                    id="adults"
                    name="adults"
                    value={formData.adults}
                    onChange={handleChange}
                    className="appearance-none border-none rounded w-14 p-2 text-black bg-[#E9EFFB] text-2xl font-bold text-center "
                  /> */}
                    <div className="appearance-none border-none rounded w-14 p-2 text-black bg-[#E9EFFB] text-2xl font-bold text-center ">
                      {room.adults}
                    </div>
                    <button
                      type="button"
                      onClick={() => handleIncrement("adults", index)}
                      className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                    >
                      +
                    </button>
                  </div>
                </div>

                {/* Include similar buttons and inputs for room, travelClass, children */}
                <div className=" flex flex-col justify-start border-2 p-2 rounded-md bg-[#E9EFFB]">
                  <label
                    className="block text-gray-700 text-base font-semibold mb-2 border-b-2  border-black pb-1"
                    htmlFor="children"
                  >
                    Children
                    <p className=" text-xs text-gray-600">Ages 2 to 17</p>
                  </label>
                  <div className="flex items-center">
                    <button
                      type="button"
                      onClick={() => handleDecrement("children", index)}
                      className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                    >
                      -
                    </button>
                    {/* <input
                    type="text"
                    inputMode="numeric" // Set input mode to numeric
                    pattern="[0-10]*" // Allow only numeric input
                    id="children"
                    name="children"
                    value={formData.children}
                    onChange={handleChange}
                    className="appearance-none border-none rounded w-14 p-2 text-black bg-[#E9EFFB] text-2xl font-bold text-center"
                  /> */}
                    <div className="appearance-none border-none rounded w-14 p-2 text-black bg-[#E9EFFB] text-2xl font-bold text-center">
                      {room.children}
                    </div>
                    <button
                      type="button"
                      onClick={() => handleIncrement("children", index)}
                      className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>
              <hr />
            </div>
          ))}
        </div>
        <hr />

        {/* ... (continue with other fields if needed) */}
        <div className=" flex gap-5 mt-2 p-2">
          <label className="block text-gray-700 text-2xl font-bold mb-2">
            Total Travellers:
          </label>
          <p className=" text-2xl">{calculateTotalTravelers()}</p>
        </div>
        <hr />

        <button
          type="button"
          onClick={handleAddRoom}
          className=" uppercase text-base font-semibold text-blue-600 "
        >
          Add Another Room
        </button>

        <div className="flex justify-end mt-3">
          <button
            type="button"
            onClick={handleCancel}
            className="px-4 py-2 mr-2 bg-red-500 text-white rounded-md"
          >
            Cancel
          </button>
          <button
            type="button"
            onClick={handleApply}
            className="px-4 py-2 bg-[#00C590] text-white rounded-md"
          >
            Apply
          </button>
        </div>
      </div>
    </div>
  );
};

export default TravelersAndClassForm;
