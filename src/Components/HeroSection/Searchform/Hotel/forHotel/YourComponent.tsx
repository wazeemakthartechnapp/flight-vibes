"use client";
import React, { useState } from "react";
import TravelersAndClassForm from "./Travelers";

const OriginalComponentHotel: React.FC = () => {
  const [showTravelersAndClassForm, setShowTravelersAndClassForm] =
    useState(false);
  const [formData, setFormData] = useState({
    travelers: "7",
    room: "1",
    travelClass: "Economy",
    adults: "3",
    children: "4",
  });

  const handleOpenForm = () => {
    setShowTravelersAndClassForm(true);
  };

  const handleCloseForm = () => {
    setShowTravelersAndClassForm(false);
  };

  const handleFormChange = (newData: { [key: string]: string }) => {
    setFormData((prevData) => ({ ...prevData, ...newData }));
  };

  const calculateTotalTravelers = (): string => {
    const adults = parseInt(formData.adults, 10) || 0;
    const children = parseInt(formData.children, 10) || 0;
    return (adults + children).toString();
  };

  return (
    <>
      <div
        className="col-span-3 row-span-2 h-full rounded-lg bg-[#E9EFFB] p-4 flex flex-col cursor-pointer"
        onClick={handleOpenForm}
      >
        <p className="text-gray-500 text-sm flex items-center">
          Travelers & Class
        </p>
        <div className="flex flex-col gap-2">
          <p className="mt-4 font-semibold text-2xl text-blue-950">
            {calculateTotalTravelers()} Travellers,<br /> {formData.room} Rooms 
          </p>
          <p className="text-gray-500 md:mt-12 mt-0">
            {formData.adults} Adults, {formData.children} Children
          </p>
        </div>
      </div>

      {showTravelersAndClassForm && (
        <TravelersAndClassForm
          formData={formData}
          onChange={handleFormChange}
          onClose={handleCloseForm}
        />
      )}
    </>
  );
};

export default OriginalComponentHotel;
