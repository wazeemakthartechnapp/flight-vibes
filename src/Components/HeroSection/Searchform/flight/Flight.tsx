"use client";
import React, { useRef, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { Typography, Checkbox, Container } from "@mui/material";
import FlightTakeoffIcon from "@mui/icons-material/FlightTakeoff";
import FlightLandIcon from "@mui/icons-material/FlightLand";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import SearchIcon from "@mui/icons-material/Search";
import "react-datepicker/dist/react-datepicker.css";
import { RxDividerVertical } from "react-icons/rx";
import { DateRange } from "react-date-range";
import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import { BsFillTriangleFill } from "react-icons/bs";
import { GoDotFill } from "react-icons/go";
import { MdOutlineWbSunny } from "react-icons/md";
import { WiRainMix } from "react-icons/wi";
import "./YourComponent.css";
import OriginalComponentFlight from "./ForFlight/YourComponent";




interface DateChangeParams {
  startDate: Date | null;
  endDate: Date | null;
}

const Flight = () => {
  const {
    control,
    handleSubmit,
    setValue,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any) => {
    alert(JSON.stringify(data));
    // Displaying form data for demonstration purposes
    const formattedStartDate = dateRange[0].startDate.toLocaleDateString();
    const formattedEndDate = dateRange[0].endDate.toLocaleDateString();

    // Display the formatted dates in the alert
    alert(`Selected Date Range: ${formattedStartDate} to ${formattedEndDate}`);
  };

  const [dateRange, setDateRange] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);

  const [showCalendar, setShowCalendar] = useState(false);
  const dateRangeRef = useRef(null);

  const handleDateChange = (ranges: any) => {
    const { startDate, endDate } = ranges.selection;
    setDateRange([ranges.selection]);

    // Example: Log the start and end dates to the console
    console.log("Start Date:", startDate);
    console.log("End Date:", endDate);

   
  };

  const toggleCalendar = () => {
    setShowCalendar(!showCalendar);
  };

  const formatDay = (date: Date) => {
    return date.getDate();
  };

  const [showTravelersAndClassForm, setShowTravelersAndClassForm] = useState(false);

  const [buttonText, setButtonText] = useState('Return');

  const handleClick = () => {
    setButtonText(buttonText === 'Return' ? 'One Way' : 'Return');
  };

  return (
    <>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="md:grid relative hidden md:grid-cols-12 md:grid-rows-2 gap-2 w-full bg-white rounded-lg shadow-lg p-2"
      >
        {/* ... Other form fields ... */}

        <div className="col-span-3">
          <div className="flex justify-between px-4 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
            <div className="flex flex-col gap-1 flex-grow">
              <p className="text-sm text-gray-500">From</p>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  {...register("From")}
                  type="text"
                  className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                />
                <label
                  htmlFor="floting_Destination_Airport"
                  className="absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Destination / Airport
                </label>
              </div>
            </div>
            <FlightTakeoffIcon className="object-right" />
          </div>
        </div>

        <button className=" absolute rounded-full px-3 py-3 bg-[#00C590] mt-[5.5rem] ml-[10.75rem]">
          <SwapVertIcon className=" fill-white" />
        </button>

        <div
          onClick={toggleCalendar}
          className="col-span-2 row-span-2 rounded-lg bg-[#E9EFFB] p-4"
        >
          <div className="flex flex-col gap-1">
            <div className="flex justify-between items-center">
              <p className="text-sm text-gray-500 flex items-center">
                Depart on
              </p>

              <div>
                <BsFillTriangleFill className="text-3xl text-orange-800 bg-red-500 p-2 bg-opacity-40 rounded-full  " />
              </div>
            </div>
            <p className="text-6xl text-[#013365] font-semibold">
              {formatDay(dateRange[0].startDate)}
            </p>

            <p className="text-md text-gray-500">
              {dateRange[0].startDate.toDateString()}
            </p>
          </div>
          <div className="flex items-center justify-between">
            <div>
              <WiRainMix className="text-5xl text-blue-900 bg-none" />
            </div>
            <div className="flex flex-col md:mt-4">
              <p className="text-xl">29.6 C</p>
              <Typography
                sx={{ fontSize: 14 }}
                color="text.secondary"
                gutterBottom
              >
                Moderate Rain
              </Typography>
            </div>
          </div>
        </div>
        {/* ... Other form fields ... */}

        <div
          onClick={toggleCalendar}
          className="col-span-2 row-span-2 rounded-lg bg-[#E9EFFB] p-4"
        >
          <div className="flex flex-col gap-1">
            <div className=" flex justify-between items-center">
              <p className=" text-sm text-gray-500 flex items-cente">
                Return On
              </p>

              <div className=" gap-2 flex">
                <BsFillTriangleFill className="text-3xl text-yellow-500 bg-yellow-500 p-2 bg-opacity-40 rounded-full" />

                <GoDotFill
                  fontSize="medium"
                  className="text-3xl text-blue-800 bg-blue-400 rounded-full"
                />
              </div>
            </div>
            <p className="text-6xl text-[#013365] font-semibold">
              {formatDay(dateRange[0].endDate)}
            </p>

            <p  {...register("From")} className="text-md text-gray-500">
              {dateRange[0].endDate.toDateString()}
            </p>

          </div>
          <div className=" flex gap-1 justify-between items-center md:mt-4">
            <div>
              <MdOutlineWbSunny className="text-5xl text-orange-500" />
            </div>
            <div>
              <p className="text-xl">29.6 C</p>
              <Typography
                sx={{ fontSize: 14 }}
                color="text.secondary"
                gutterBottom
              >
                Moderate Rain
              </Typography>
            </div>
          </div>
        </div>

        {/* ... Other form fields ... */}
        <div className="col-span-3 row-span-2 flex flex-col">
          <OriginalComponentFlight/>
        </div>
        {/* ... Other form fields ... */}

        <div className="col-span-2">
          <div className="">
            <div className="flex items-center">
              <div onClick={handleClick} className="flex justify-between">
                Return
                <KeyboardArrowDownIcon />
              </div>
              <div>
                <RxDividerVertical size="25" />
              </div>
              <div className="flex justify-between items-center">
                <Checkbox
                  {...register("direct")}
                  size="small"
                  color="success"
                  defaultChecked
                />
                <Typography>Direct</Typography>
              </div>
            </div>
          </div>
        </div>

        {/* ... Other form fields ... */}

        <div className="col-span-3">
          <div className="flex justify-between px-4 items-center pt-2 rounded-lg bg-[#E9EFFB] h-full">
            <div className="flex flex-col gap-1 flex-grow">
              <p className="text-sm text-gray-500">To</p>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  {...register("To")}
                  type="text"
                  className="flex flex-grow py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                />
                <label
                  htmlFor="floting_Destination_Airport"
                  className="absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Destination / Airport
                </label>
              </div>
            </div>
            <FlightLandIcon className="object-right" />
          </div>
        </div>

        {/* ... Other form fields ... */}

        <div className="col-span-2 rounded-lg bg-blue-900 -mt-16 flex justify-center items-center">
          <button
            type="submit"
            className="flex justify-center items-center text-white py-16 gap-3"
          >
            <SearchIcon fontSize="large" />
            <Typography variant="h5">Search</Typography>
          </button>
        </div>
        {showCalendar && (
          <div className="modal-overlay">
            <div className="modal-content bg-white p-8 rounded shadow-md flex flex-col justify-end">
              <Controller
                control={control}
                name="departDate"
                render={({ field }) => (
                  <DateRange
                    ranges={dateRange}
                    onChange={handleDateChange}
                    months={2}
                    direction="horizontal"
                    moveRangeOnFirstSelection={false}
                    editableDateInputs={true}
                  />
                )}
              />
              <button
                onClick={toggleCalendar}
                className="mt-4 bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none"
              >
                Apply Date
              </button>
            </div>
          </div>
        )}
      </form>
    </>
  );
};

export default Flight;
