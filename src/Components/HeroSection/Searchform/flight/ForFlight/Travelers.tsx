// TravelersAndClassModal.tsx
"use client";
import React, { useState, useEffect, useRef } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { RiCloseFill } from "react-icons/ri";

interface TravelersAndClassFormProps {
  formData: {
    travelers: string;
    room: string;
    travelClass: string;
    adults: string;
    children: string;
  };
  onChange: (data: { [key: string]: string }) => void;
  onClose: () => void;
}

const TravelersAndClassForm: React.FC<TravelersAndClassFormProps> = ({
  formData,
  onChange,
  onClose,
}) => {
  const formRef = useRef<HTMLDivElement>(null);

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    onChange({ ...formData, [e.target.name]: e.target.value });
  };

  const handleIncrement = (field: string) => {
    const currentValue = parseInt(formData[field], 10) || 0;
    onChange({ ...formData, [field]: (currentValue + 1).toString() });
  };

  const handleDecrement = (field: string) => {
    const currentValue = parseInt(formData[field], 10) || 0;
    if (currentValue > 0) {
      onChange({ ...formData, [field]: (currentValue - 1).toString() });
    }
  };

  const calculateTotalTravelers = (): string => {
    const adults = parseInt(formData.adults, 10) || 0;
    const children = parseInt(formData.children, 10) || 0;
    return (adults + children).toString();
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // Handle form submission logic here
    console.log("Form submitted:", formData);
    onClose(); // Close the form after submission
  };

  // Add event listener to close the form when clicking outside of it
  useEffect(() => {
    const handleOutsideClick = (e: MouseEvent) => {
      if (formRef.current && !formRef.current.contains(e.target as Node)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  const handleCancel = () => {
    // Reset the form data or perform any other cancel logic
    onClose();
  };

  const handleApply = () => {
    // You can perform any additional logic before closing the form
    onClose();
  };

  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div ref={formRef} className="bg-white p-4 rounded-lg">
        <div className="flex justify-end mb-4">
          
          <button
            type="button"
            onClick={onClose}
            className="px-4 py-2 bg-gray-500 text-white rounded-md"
          >
            <RiCloseFill />
          </button>
        </div>
        <form onSubmit={handleSubmit}>
          <div className=" flex gap-4 justify-around">
            <div className=" flex flex-col justify-start border-2 p-4 rounded-md bg-[#E9EFFB]">
              <label
                className="block text-gray-700 text-2xl font-bold mb-2 border-b-2  border-black pb-1"
                htmlFor="travelers"
              >
                Adults
              </label>
              <div className="flex items-center">
                <button
                  type="button"
                  onClick={() => handleDecrement("adults")}
                  className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                >
                  -
                </button>
                <input
                  type="text"
                  inputMode="numeric" // Set input mode to numeric
                  pattern="[0-10]*" // Allow only numeric input
                  id="adults"
                  name="adults"
                  value={formData.adults}
                  onChange={handleChange}
                  className="appearance-none border-none rounded w-14 p-2 text-black bg-[#E9EFFB] text-2xl font-bold text-center "
                />
                <button
                  type="button"
                  onClick={() => handleIncrement("adults")}
                  className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                >
                  +
                </button>
              </div>
            </div>

            {/* Include similar buttons and inputs for room, travelClass, children */}
            <div className="  flex flex-col justify-start border-2 p-4 rounded-md bg-[#E9EFFB]">
              <label
                className="block text-gray-700 text-2xl font-bold mb-2 border-b-2  border-black pb-1"
                htmlFor="children"
              >
                Children
              </label>
              <div className="flex items-center">
                <button
                  type="button"
                  onClick={() => handleDecrement("children")}
                  className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                >
                  -
                </button>
                <input
                  type="text"
                  inputMode="numeric" // Set input mode to numeric
                  pattern="[0-10]*" // Allow only numeric input
                  id="children"
                  name="children"
                  value={formData.children}
                  onChange={handleChange}
                  className="appearance-none border-none rounded w-14 p-2 bg-[#E9EFFB] text-black text-2xl font-bold text-center"
                />
                <button
                  type="button"
                  onClick={() => handleIncrement("children")}
                  className="bg-[#00C590] px-4 py-1 rounded-md text-[1.5rem] text-white text-center border border-white"
                >
                  +
                </button>
              </div>
            </div>
          </div>

          <div className=" flex flex-col justify-start border-2 p-4 rounded-md bg-[#E9EFFB] mt-4">
            <label
              className="block text-gray-700 text-2xl font-bold mb-2 border-b-2  border-black pb-1"
              htmlFor="travelClass"
            >
              Travel Class
            </label>
            <select
              id="travelClass"
              name="travelClass"
              value={formData.travelClass}
              onChange={handleChange}
              className="appearance-none rounded w-full py-2 px-3 bg-[#E9EFFB] text-gray-700 text-xl"
            >
              <option value="Economy">Economy</option>
              <option value="Business">Business</option>
              <option value="First Class">First Class</option>
            </select>
          </div>
          {/* ... (continue with other fields if needed) */}
          <div className=" flex gap-5 mt-2 p-2">
            <label className="block text-gray-700 text-2xl font-bold mb-2">
              Total Travelers:
            </label>
            <p className=" text-2xl">{calculateTotalTravelers()}</p>
          </div>

          <div className="flex justify-end">
            <button
              type="button"
              onClick={handleCancel}
              className="px-4 py-2 mr-2 bg-red-500 text-white rounded-md"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={handleApply}
              className="px-4 py-2 bg-[#00C590] text-white rounded-md"
            >
              Apply
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default TravelersAndClassForm;
