"use client";
import React from "react";
import SearchForm from "./Searchform/Buttonstog";
import SearchFormtab2 from "./Searchformtab2";
import BottomNav from "./BottomNav";
import { Container } from "@mui/material";

const Herosection = () => {
  return (
    <div className=" ">
      <div
        className=" bg-[url('/Images/Hero/Hero1.jpg')] 
                  mt-2  bg-no-repeat bg-center bg-cover 

                  w-full
          h-screen
          max-h-[30rem]
          md:pt-24
          pt-3
          relative
          block
          z-10

                  before:content-['']
                  before:absolute
                  before:inset-0
                  before:block
                  before:bg-gradient-to-l
                before:from-pink-400 from-33.3%
                before:via-blue-600 via-33.3% 
                before:to-emerald-400 to-33.3% 
                  before:opacity-60
                  before:z-[-5] "
      >
        <Container className=" px-0">
          <SearchForm />
          <SearchFormtab2 />
        </Container>
        <BottomNav />
      </div>
    </div>
  );
};

export default Herosection;
