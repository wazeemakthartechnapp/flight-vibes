import Header from "@/Components/Header/Header";
import React from "react";
import Bookaflight from "@/Components/BookAFlight/Bookaflight";
import Destinationsearch from "@/Components/DestinationSearch/Destinationsearch";
import Membersonly from "@/Components/MembersOnly/Membersonly";
import Footer from "@/Components/Footer/Footer";
import Herosection from "@/Components/HeroSection/Herosection";
import AnimatedAccordionPage from "@/Components/FAQ/Accordianpage";
import Popular from "@/Components/PopularDestination/popular";
import Popularphone from "@/Components/PopularDestination/Popularphone";
import Carousel from "@/Components/PlaneyourTrip/Carasol";
// import SearchForm2 from "@/Components/HeroSection/Searchform2";



const page = () => {
  return (
    <div>
      <Header />
      <Herosection />
      {/* <SearchForm2/> */}
      <Bookaflight />
      <Carousel/>
      <Destinationsearch />
      <Popular />
      <Popularphone/>
      <AnimatedAccordionPage />
      <Membersonly />
      <Footer />
    </div>
  );
};

export default page;
